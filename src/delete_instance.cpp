#include "delete_instance.h"
#include "utils.h"
#include <algorithm>
#include <iostream>
#include <vector>


using namespace std;

void DeleteInstanceImpl::deleteinstance(int g, Lattice &l, _Context &ctx) {
    vector<int> g_intent;
    for (int j = 0; j < l.ctx->M.size(); ++j)
        if (l.ctx->I[g][j])
            g_intent.push_back(j);

    std::map<Concept *, std::vector<int>> common_attributes =
        get_common_attributes(g_intent, l._concepts);                                     // O(|L||M|log|M|)
    Concept *min_concept = get_maximal_I_concept(g_intent, l._bottom, common_attributes); // O(|M|max(g')\M\)
    DestructorByConcept affected_concepts_hashmap = {{min_concept, nullptr}};
    set<Concept *> processed = {};
    vector<Concept *> affected_concepts = {min_concept};
    while (!affected_concepts.empty()) {
        auto c = affected_concepts.back();
        affected_concepts.pop_back();
        processed.insert(c);
        erase_number(c->X, g);
        for (auto parent : c->parents) {
            if (processed.find(parent) == processed.end()) {
                affected_concepts.push_back(parent);
                affected_concepts_hashmap.insert({parent, nullptr});
            }
        }
    }

    fast_deletion(min_concept, affected_concepts_hashmap, l);

    l.ctx->G.erase(l.ctx->G.begin() + g);
    l.ctx->I.erase(l.ctx->I.begin() + g);

    for (auto c : l._concepts) {
        if (c->X.size() == l.ctx->G.size())
            l._top = c;

        if (c->Y.size() == l.ctx->M.size())
            l._bottom = c;

        for (int i = 0; i < c->X.size(); ++i)
            if (c->X[i] > g)
                c->X[i]--;
    }
}


void DeleteInstanceImpl::fast_deletion(Concept *c, DestructorByConcept &affected_concepts, Lattice &l) {
    set<Concept *> deleted, modified;
    fast_deletion(c, affected_concepts, deleted, modified, l);
    for (auto c : deleted) {
        // for (auto it = c->parents.begin(); it != c->parents.end(); ++it) {
        //     (*it)->children.erase(std::find((*it)->children.begin(), (*it)->children.end(), c));
        // }
        // for (auto it = c->children.begin(); it != c->children.end(); ++it) {
        //     (*it)->parents.erase(std::find((*it)->parents.begin(), (*it)->parents.end(), c));
        // }
        delete c;
    }
}


void DeleteInstanceImpl::fast_deletion(Concept *c,
                                       DestructorByConcept &affected_concepts,
                                       set<Concept *> &deleted,
                                       set<Concept *> &modified,
                                       Lattice &l) {
    if (confirm_deletion(c, affected_concepts)) {
        Concept *destructor_node = affected_concepts.at(c);
        deleted.insert(c);
        remove_link(c, destructor_node);
        l._concepts.erase(std::find(l._concepts.begin(), l._concepts.end(), c));
        for (auto parent : c->parents) {
            if (deleted.find(parent) != deleted.end() || modified.find(parent) != modified.end())
                continue;
            else
                fast_deletion(parent, affected_concepts, deleted, modified, l);
        }
        std::vector<std::list<Concept *>::iterator> parents_to_delete;
        for (auto it = c->parents.begin(); it != c->parents.end(); ++it) {
            auto parent = *it;
            if (modified.find(parent) != modified.end()) {
                parents_to_delete.push_back(it);
                parent->children.erase(std::find(parent->children.begin(), parent->children.end(), c));
                bool covered = false;
                for (auto child : parent->children) {
                    if (is_old_concept(child, affected_concepts) && is_included(child->Y, destructor_node->Y)) {
                        covered = true;
                        break;
                    }
                }
                if (!covered) {
                    set_link(parent, destructor_node);
                }
            }
        }
        for (auto it : parents_to_delete)
            c->parents.erase(it);
    } else {
        modified.insert(c);
        for (auto parent : c->parents)
            modified.insert(parent);
    }
}


bool DeleteInstanceImpl::is_old_concept(Concept *c, DestructorByConcept &affected_concepts) {
    return affected_concepts.find(c) == affected_concepts.end();
}


bool DeleteInstanceImpl::confirm_deletion(Concept *c, DestructorByConcept &affected_concepts) {
    int old_children = 0;
    Concept *destructor_node;
    for (auto child : c->children) {
        if (is_old_concept(child, affected_concepts)) {
            if (old_children == 0) {
                destructor_node = child;
                old_children = 1;
            } else {
                old_children = 2;
                break;
            }
        }
    }
    if (old_children == 1 && destructor_node->X == c->X) {
        auto it = affected_concepts.find(c);
        if (it != affected_concepts.end()) {
            affected_concepts.erase(it);
        }
        affected_concepts.insert({c, destructor_node});
        return true;
    } else {
        return false;
    }
}