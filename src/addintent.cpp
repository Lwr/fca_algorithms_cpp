#include "addintent.h"
#include "utils.h"
#include <iostream>
#include <list>

Concept *AddIntentImpl::recursive_addintent(const std::vector<int> &intent,
                                            Concept *generator_concept,
                                            Lattice &l,
                                            _Context &ctx,
                                            std::map<Concept *, std::vector<int>> &common_attributes) {
    /*
:param intent: the intent of a new concept
:param generator_concept: pre-computed generator concept s.t., intent \subseteq generator_concept
:param l: a lattice diagram
:return: a concept C = (A,B) such that B = intent and A = B'.
*/
    Concept *gen_concept = get_maximal_I_concept(intent, generator_concept, common_attributes);
    if (is_included(gen_concept->Y, intent) && is_included(intent, gen_concept->Y))
        return gen_concept;

    std::list<Concept *> generator_parents = get_parents(gen_concept);

    std::list<Concept *> new_parents;
    std::vector<std::list<Concept *>::iterator> new_parents_to_delete;
    for (auto iter = generator_parents.begin(); iter != generator_parents.end(); ++iter) {
        auto candidate = *iter;
        Concept *candidate_so_far = candidate;
        std::vector<int> candidate_intent = candidate_so_far->Y;
        if (!is_strictly_included(candidate_intent, intent, common_attributes[candidate_so_far].size())) {
            candidate_so_far =
                recursive_addintent(common_attributes[candidate_so_far], candidate, l, ctx, common_attributes);
        }
        candidate_intent = candidate_so_far->Y;
        bool add_parent = true;
        new_parents_to_delete.clear();
        for (auto it = new_parents.begin(); it != new_parents.end(); ++it) {
            Concept *parent = *it;
            std::vector<int> parent_intent = parent->Y;
            if (is_included(candidate_intent, parent_intent)) {
                add_parent = false;
            } else if (is_included(parent_intent, candidate_intent)) {
                new_parents_to_delete.push_back(it);
                // it = new_parents.erase(it);
            }
        }

        for (auto it : new_parents_to_delete)
            new_parents.erase(it);


        if (add_parent) {
            new_parents.push_back(candidate_so_far);
        }
    }

    Concept *new_concept = new Concept(ctx, gen_concept->X, intent);
    l._concepts.push_back(new_concept);
    if (new_concept->X.size() == ctx.G.size())
        l._top = new_concept;

    if (new_concept->Y.size() == ctx.M.size())
        l._bottom = new_concept;

    for (auto parent : new_parents) {
        remove_link(parent, gen_concept);
        set_link(parent, new_concept);
    }
    set_link(new_concept, gen_concept);
    common_attributes.insert({new_concept, common_attributes.at(gen_concept)});
    return new_concept;
}

Concept *AddIntentImpl::addintent(const std::vector<int> &intent,
                                  Concept *generator_concept,
                                  Lattice &l,
                                  _Context &ctx) {
    std::vector<int> sorted_intent(intent);
    std::sort(sorted_intent.begin(), sorted_intent.end());
    std::map<Concept *, std::vector<int>> common_attributes = get_common_attributes(sorted_intent, l._concepts);
    return recursive_addintent(sorted_intent, generator_concept, l, ctx, common_attributes);
}
