#include "solver.h"
#include "base_models.h"
#include "inclose.h"
#include <iostream>


bool has_bottom(_Context &ctx, std::vector<Concept *> &concepts) {
    for (Concept *c : concepts)
        if (c->Y.size() == ctx.M.size())
            return true;
    return false;
}

Concept *calculate_bottom(_Context &ctx) {
    std::vector<int> objects_with_all_attributes = {};
    for (std::size_t i = 0; i < ctx.G.size(); ++i) {
        bool is_related_to_all = true;
        for (std::size_t j = 0; j < ctx.M.size(); ++j) {
            if (!ctx.I[i][j]) {
                is_related_to_all = false;
                break;
            }
        }
        if (is_related_to_all)
            objects_with_all_attributes.push_back(i);
    }
    std::vector<int> all_attributes = {};
    for (std::size_t j = 0; j < ctx.M.size(); ++j)
        all_attributes.push_back(j);
    return new Concept(ctx, objects_with_all_attributes, all_attributes);
}


std::vector<Concept *> IncloseSolver::get_concepts(std::vector<std::string> &p_G,
                                                   std::vector<std::string> &p_M,
                                                   std::vector<std::vector<int>> &p_I) {
    _Context ctx = _Context(p_G, p_M, p_I);
    auto res = inclose(ctx);
    if (!has_bottom(ctx, res)) {
        res.push_back(calculate_bottom(ctx));
    }
    return res;
}

std::vector<Concept *> IncloseSolver::get_concepts(_Context &p_ctx) {
    auto res = inclose(p_ctx);
    if (!has_bottom(p_ctx, res))
        res.push_back(calculate_bottom(p_ctx));
    return res;
}
