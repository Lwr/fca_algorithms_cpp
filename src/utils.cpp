#include "utils.h"
#include <iostream>


template <class T>
void erase_element_from_vector(std::vector<T> &v, T elem) {
    v.erase(find(v.begin(), v.end(), elem));
}


template <class T>
void erase_element_from_list(std::list<T> &v, T elem) {
    v.erase(find(v.begin(), v.end(), elem));
}

std::list<Concept *> get_parents(Concept *generator_concept) {
    return generator_concept->parents;
}


std::list<Concept *> get_children(Concept *generator_concept) {
    return generator_concept->children;
}


Concept *get_maximal_concept(std::vector<int> intent_or_extent,
                             Concept *generator_concept,
                             const std::map<Concept *, std::vector<int>> &common_elements,
                             bool for_intent) {
    bool elem_is_maximal = true;
    Concept *res = generator_concept;
    while (elem_is_maximal) {
        elem_is_maximal = false;
        std::list<Concept *> neighbors = for_intent ? get_parents(res) : get_children(res);
        for (auto neighbor : neighbors) {
            if (common_elements.at(neighbor).size() == intent_or_extent.size()) {
                res = neighbor;
                elem_is_maximal = true;
                break;
            }
        }
    }
    return res;
}


std::map<Concept *, std::vector<int>> get_common_elements(std::vector<int> intent_or_extent,
                                                          std::vector<Concept *> &l,
                                                          bool for_attributes) {
    std::map<Concept *, std::vector<int>> common;
    for (int idx = 0; idx < l.size(); ++idx) {
        auto c = l[idx];
        std::vector<int> common_elements;
        for (int i : intent_or_extent) {
            if (is_there(for_attributes ? c->Y : c->X, i))
                common_elements.push_back(i);
        }
        common.insert({c, common_elements});
    }
    return common;
}


Concept *get_maximal_I_concept(std::vector<int> intent,
                               Concept *generator_concept,
                               const std::map<Concept *, std::vector<int>> &common_attributes) {
    return get_maximal_concept(intent, generator_concept, common_attributes, true);
}

Concept *get_maximal_E_concept(std::vector<int> extent,
                               Concept *generator_concept,
                               const std::map<Concept *, std::vector<int>> &common_objects) {
    return get_maximal_concept(extent, generator_concept, common_objects, false);
}


std::map<Concept *, std::vector<int>> get_common_attributes(std::vector<int> intent, std::vector<Concept *> &l) {
    return get_common_elements(intent, l, true);
}


std::map<Concept *, std::vector<int>> get_common_objects(std::vector<int> extent, std::vector<Concept *> &l) {
    return get_common_elements(extent, l, false);
}


bool is_there(const std::vector<int> &sorted_array, int elem) {
    auto low = std::lower_bound(sorted_array.begin(), sorted_array.end(), elem);
    return low != sorted_array.end() && *low == elem;
}


bool is_strictly_included(const std::vector<int> &set, const std::vector<int> &other_set, int common_attributes) {
    return common_attributes < other_set.size() && common_attributes == set.size();
}

bool is_included(const std::vector<int> &set, const std::vector<int> &other_set) {
    // both are considered to be ordered vectors
    for (int i : set) {
        if (!is_there(other_set, i))
            return false;
    }
    return true;
}

void remove_link(Concept *node, Concept *other_node) {
    node->remove_child(other_node);
}


void set_link(Concept *node, Concept *other_node) {
    node->add_child(other_node);
}

void remove_node(Concept *node, Lattice &l) {
    for (auto c : node->parents)
        erase_element_from_list(c->children, node);

    for (auto c : node->children)
        erase_element_from_list(c->parents, node);

    erase_element_from_vector(l._concepts, node);
}


void erase_number(std::vector<int> &vector, int i) {
    int idx = 0;
    for (; idx < vector.size(); ++idx) {
        if (vector[idx] == i)
            break;
    }
    if (idx < vector.size())
        vector.erase(vector.begin() + idx);
}


void add_ordered(std::vector<int> &sorted_array, int elem) {
    auto low = std::lower_bound(sorted_array.begin(), sorted_array.end(), elem);
    sorted_array.insert(low, elem);
}

void add_ordered_if_not_present(std::vector<int> &sorted_array, int elem) {
    auto low = std::lower_bound(sorted_array.begin(), sorted_array.end(), elem);
    if (!(low != sorted_array.end() && *low == elem))
        sorted_array.insert(low, elem);
}

std::vector<int> intersection(std::vector<int> &sorted_array_1, std::vector<int> &sorted_array_2) {
    std::vector<int> res;
    std::vector<int> v1 = sorted_array_2, v2 = sorted_array_1;
    if (sorted_array_1.size() < sorted_array_2.size()) {
        v1 = sorted_array_1;
        v2 = sorted_array_2;
    }
    for (auto elem : v1)
        if (is_there(v2, elem))
            res.push_back(elem);

    return res;
}


Concept *get_maximal_I_concept(std::vector<int> intent, Concept *generator_concept) {
    bool elem_is_maximal = true;
    Concept *res = generator_concept;
    while (elem_is_maximal) {
        elem_is_maximal = false;
        std::list<Concept *> neighbors = get_parents(res);
        for (auto neighbor : neighbors) {
            if (intersection(neighbor->Y, intent).size() == intent.size()) {
                res = neighbor;
                elem_is_maximal = true;
                break;
            }
        }
    }
    return res;
}

bool unordered_map_contains(std::unordered_map<std::string, int> m, std::string key) {
    return m.find(key) != m.end();
}

bool unordered_map_contains_int(std::unordered_map<int, int> m, int key) {
    return m.find(key) != m.end();
}
