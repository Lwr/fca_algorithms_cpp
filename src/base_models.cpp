#include "base_models.h"
#include <algorithm>
#include <iostream>

using array = std::vector<int>;

// PYBIND11_MAKE_OPAQUE(std::vector<int>);
// PYBIND11_MAKE_OPAQUE(std::map<std::string, double>);

_Context::_Context(std::vector<std::string> &p_G, std::vector<std::string> &p_M, std::vector<std::vector<int>> &p_I) {
    G = p_G;
    M = p_M;
    I = p_I;
}

std::string _Context::__repr__() {
    std::string s = "G: ({ ";
    if (G.size() > 0) {
        for (int i = 0; i < G.size() - 1; ++i) {
            std::string obj = G[i];
            s += obj;
            s += ", ";
        }

        s += G.back();
        s += " ";
    }

    s += "}, M: {";

    if (M.size() > 0) {
        for (int j = 0; j < M.size() - 1; ++j) {
            std::string obj = M[j];
            s += obj;
            s += ", ";
        }

        s += M.back();
        s += " ";
    }

    s += "})\nI: {\n";

    for (int i = 0; i < G.size(); ++i) {
        s += "  {";
        for (int j = 0; j < M.size() - 1; ++j) {
            std::string obj = I[i][j] ? "1" : "0";
            s += obj;
            s += ", ";
        }
        s += I[i].back() ? "1" : "0";
        s += "},\n";
    }

    s += "}\n";

    return s;
}


Concept::Concept(_Context &p_ctx, const std::vector<int> &p_X, const std::vector<int> &p_Y) : ctx(p_ctx) {
    X = p_X;
    Y = p_Y;
}

std::vector<std::string> Concept::hr_X() {
    std::vector<std::string> obj;
    for (auto idx : X)
        obj.push_back(ctx.G[idx]);
    return obj;
}


std::vector<std::string> Concept::hr_Y() {
    std::vector<std::string> attr;
    for (auto idx : Y)
        attr.push_back(ctx.M[idx]);
    return attr;
}


std::pair<std::vector<std::string>, std::vector<std::string>> Concept::to_tuple() {
    return {hr_X(), hr_Y()};
}

std::string Concept::__repr__() {
    std::string s = "({ ";
    const auto objs_and_attrs = to_tuple();
    if (objs_and_attrs.first.size() > 0) {
        for (int i = 0; i < objs_and_attrs.first.size() - 1; ++i) {
            std::string obj = objs_and_attrs.first[i];
            s += obj;
            s += ", ";
        }

        s += objs_and_attrs.first.back();
        s += " ";
    }

    s += "}, { ";

    if (objs_and_attrs.second.size() > 0) {
        for (int j = 0; j < objs_and_attrs.second.size() - 1; ++j) {
            std::string obj = objs_and_attrs.second[j];
            s += obj;
            s += ", ";
        }

        s += objs_and_attrs.second.back();
        s += " ";
    }

    s += "})";

    return s;
}


void Concept::add_child(Concept *child) {
    auto child_it_to_add = std::find(children.begin(), children.end(), child);
    if (child_it_to_add == children.end())
        children.push_back(child);

    auto parent_it_to_add = std::find(child->parents.begin(), child->parents.end(), this);
    if (parent_it_to_add == child->parents.end())
        child->parents.push_back(this);
}


void Concept::remove_child(Concept *child) {
    auto child_it_to_erase = std::find(children.begin(), children.end(), child);
    if (child_it_to_erase != children.end())
        children.erase(child_it_to_erase);
    auto it_to_erase = std::find(child->parents.begin(), child->parents.end(), this);
    if (it_to_erase != child->parents.end())
        child->parents.erase(it_to_erase);
}


// PYBIND11_MODULE(base_models, m) {
//     py::class_<_Context>(m, "_Context")
//         .def(py::init([](array G, array M, std::vector<array> I){
//             return new _Context(G, M, I);
//         }));
// }
