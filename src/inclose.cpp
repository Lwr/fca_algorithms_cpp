#include "inclose.h"
#include <iostream>

using array = std::vector<Concept *>;

// PYBIND11_MAKE_OPAQUE(std::vector<int>);
// PYBIND11_MAKE_OPAQUE(std::map<std::string, double>);


class IncloseRunner {
public:
    unsigned int r_new;
    array concepts;
    _Context &ctx;

    IncloseRunner(_Context &p_ctx) : ctx(p_ctx) {
    }

    ~IncloseRunner() {
    }

    array inclose_start() {
        r_new = 0;
        concepts = array(); // No need to clear because the previous one could be being used elsewhere

        std::vector<int> all_objects_indexes = {};
        for (std::size_t i = 0; i < ctx.G.size(); ++i) {
            all_objects_indexes.push_back(i);
        }
        Concept *initial_concept = new Concept(ctx, all_objects_indexes, {});
        concepts.push_back(initial_concept);

        inclose_recursive(0, 0);
        // pop the last concept if it hasn't been closed
        if (concepts.back()->Y.size() == 0)
            concepts.pop_back();

        if (ctx.G.size() == 0) {
            // since inclose closes by object. The case where there's no object in G
            // needs to be taken care of manually.
            std::vector<int> all_attributes_indexes = {};
            for (std::size_t j = 0; j < ctx.M.size(); ++j) {
                all_attributes_indexes.push_back(j);
            }
            concepts[0]->Y = all_attributes_indexes;
        }

        return concepts;
    }

    void inclose_recursive(int r, int y) {
        /*
        Incrementally close the concept r, beginning with attribute y
        */
        r_new++;
        for (std::size_t j = y; j < ctx.M.size(); ++j) {
            if (concepts.size() <= r_new)
                concepts.push_back(new Concept(ctx, {}, {}));
            else
                concepts[r_new]->X = {};

            for (auto i : concepts[r]->X) {
                if (ctx.I[i][j]) {
                    auto is_there_pair = is_there(concepts[r_new]->X, i);
                    if (!is_there_pair.first) {
                        concepts[r_new]->X.insert(is_there_pair.second, i);
                    }
                }
            }

            if (concepts[r_new]->X.size() > 0) {
                if (concepts[r_new]->X.size() == concepts[r]->X.size()) {
                    auto is_there_pair = is_there(concepts[r]->Y, j);
                    if (!is_there_pair.first) {
                        concepts[r]->Y.insert(is_there_pair.second, j);
                    }
                } else if (is_canonical(r, j - 1)) {
                    auto new_attributes = std::vector<int>(concepts[r]->Y);
                    auto is_there_pair = is_there(new_attributes, j);
                    if (!is_there_pair.first)
                        new_attributes.insert(is_there_pair.second, j);
                    concepts[r_new]->Y = new_attributes;
                    inclose_recursive(r_new, j + 1);
                }
            }
        }
    }

    bool is_canonical(int r, int y) {
        int new_y = y;
        for (int k = concepts[r]->Y.size() - 1; k >= 0; --k) {
            for (int j = new_y; j > concepts[r]->Y[k]; --j) {
                int h = 0;
                while (h < concepts[r_new]->X.size()) {
                    if (!ctx.I[concepts[r_new]->X[h]][j])
                        break;
                    h++;
                }
                if (h == concepts[r_new]->X.size())
                    return false;
            }
            new_y = concepts[r]->Y[k] - 1;
        }

        for (int j = new_y; j >= 0; --j) {
            int h = 0;
            while (h < concepts[r_new]->X.size()) {
                if (!ctx.I[concepts[r_new]->X[h]][j])
                    break;
                h++;
            }
            if (h == concepts[r_new]->X.size())
                return false;
        }

        return true;
    }

    std::pair<bool, std::vector<int>::iterator> is_there(std::vector<int> &sorted_array, int elem) {
        std::vector<int>::iterator low = std::lower_bound(sorted_array.begin(), sorted_array.end(), elem);
        return {low != sorted_array.end() && (*low) == elem, low};
    }
};


array inclose(_Context &p_ctx) {
    return IncloseRunner(p_ctx).inclose_start();
}

// namespace py = pybind11;

// PYBIND11_MODULE(inclose, mod) {
//     mod.def("inclose_cpp", &inclose, "Recursive inclose algorithm.");
// }
