#include "intents_dict.h"
#include <iostream>


IntentDict::IntentDict() {
}


IntentDict::~IntentDict() {
    for (auto &p : intent_dicts) {
        delete p.second;
    }
}


void IntentDict::insert(std::vector<int> &intent, std::vector<int> &extent) {
    insert(intent, extent, 0);
}

void IntentDict::insert(std::vector<int> &intent, std::vector<int> &extent, int current_index) {
    if (current_index > intent.size()) {
        return;
    }

    if (current_index == intent.size()) {
        if (extents_by_intent.size() > 1) {
            std::cout << "shouldn't be happening" << std::endl;
            return;
        }
        extents_by_intent.push_back(extent);
    } else {
        int m = intent[current_index];
        auto it = intent_dicts.find(m);
        if (it == intent_dicts.end()) {
            it = intent_dicts.insert({m, new IntentDict()}).first;
        }
        (*it).second->insert(intent, extent, current_index + 1);
    }
}

std::pair<bool, std::vector<int> &> IntentDict::get(std::vector<int> &intent) {
    return get(intent, 0);
}


std::pair<bool, std::vector<int> &> IntentDict::get(std::vector<int> &intent, int current_index) {
    auto dummy_res = std::vector<int>();
    if (current_index > intent.size()) {
        return {false, dummy_res};
    }

    if (current_index == intent.size()) {
        if (extents_by_intent.size() > 0) {
            return {true, extents_by_intent.front()};
        } else {
            return {false, dummy_res};
        }
    } else {
        int m = intent[current_index];
        auto it = intent_dicts.find(m);
        if (it == intent_dicts.end()) {
            return {false, dummy_res};
        }
        return it->second->get(intent, current_index + 1);
    }
}


void IntentDict::all_elems(std::list<std::pair<std::vector<int>, std::vector<int>>> &to_fill) {
    all_elems(to_fill, {});
}

void IntentDict::all_elems(std::list<std::pair<std::vector<int>, std::vector<int>>> &to_fill,
                           std::vector<int> intent_so_far) {
    for (auto it = intent_dicts.begin(); it != intent_dicts.end(); ++it) {
        auto m = it->first;
        intent_so_far.push_back(m);
        auto id = it->second;
        id->all_elems(to_fill, intent_so_far);
        intent_so_far.pop_back();
    }

    if (extents_by_intent.size() > 0)
        to_fill.push_back({intent_so_far, extents_by_intent.front()});
}