#include "rule.h"


Rule::Rule(float p_support, float p_confidence, std::vector<std::string> p_base, std::vector<std::string> p_add)
    : support(p_support), confidence(p_confidence), base(p_base), add(p_add) {
}

Rule::~Rule() {
}


float Rule::get_support() {
    return support;
}


float Rule::get_confidence() {
    return confidence;
}


std::vector<std::string> Rule::get_base() {
    return base;
}

std::vector<std::string> Rule::get_add() {
    return add;
}


std::string Rule::__repr__() {
    std::string s = "({";
    if (base.size() > 0) {
        s += " ";
        for (int i = 0; i < base.size() - 1; ++i) {
            auto x = base[i];
            s += x + ", ";
        }
        s += base.back() + " ";
    }
    s += "} -> {";
    if (add.size() > 0) {
        s += " ";
        for (int i = 0; i < add.size() - 1; ++i) {
            auto x = add[i];
            s += x + ", ";
        }
        s += add.back() + " ";
    }
    s += "}";
    s += ", supp=" + std::to_string(support);
    s += " , conf=" + std::to_string(confidence);
    s += ")";
    return s;
}
