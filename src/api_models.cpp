#include "api_models.h"
#include "base_models.h"
#include "inclose.h"
#include "lattice.h"
#include "rule.h"
#include <pybind11/stl.h>

namespace py = pybind11;


PYBIND11_MAKE_OPAQUE(std::vector<int> &);
PYBIND11_MAKE_OPAQUE(std::vector<std::string> &);
PYBIND11_MAKE_OPAQUE(std::vector<std::vector<int>> &);


class TrampolineContext : public Context {
public:
    /* Inherit the constructors */
    using Context::Context;

    /* Trampoline (need one for each virtual function) */
    std::vector<Concept *> get_concepts() override {
        PYBIND11_OVERRIDE(std::vector<Concept *>, Context, get_concepts, );
    }
};


class TrampolineConcept : public Concept {
public:
    /* Inherit the constructors */
    using Concept::Concept;

    /* Trampoline (need one for each virtual function) */
    std::string __repr__() override {
        PYBIND11_OVERRIDE(std::string, Concept, __repr__, );
    }

    void add_child(Concept *c) override {
        PYBIND11_OVERRIDE(void, Concept, add_child, c);
    }

    void remove_child(Concept *c) override {
        PYBIND11_OVERRIDE(void, Concept, remove_child, c);
    }

    using tuple = std::pair<std::vector<std::string>, std::vector<std::string>>;
    tuple to_tuple() override {
        PYBIND11_OVERRIDE(tuple, Concept, to_tuple);
    }

    std::vector<std::string> hr_X() override {
        PYBIND11_OVERRIDE(std::vector<std::string>, Concept, hr_X);
    }


    std::vector<std::string> hr_Y() override {
        PYBIND11_OVERRIDE(std::vector<std::string>, Concept, hr_Y);
    }
};


class TrampolineRule : public Rule {
public:
    /* Inherit the constructors */
    using Rule::Rule;

    /* Trampoline (need one for each virtual function) */
    std::string __repr__() override {
        PYBIND11_OVERRIDE(std::string, Rule, __repr__, );
    }

    float get_support() override {
        PYBIND11_OVERRIDE(float, Rule, get_support, );
    }

    float get_confidence() override {
        PYBIND11_OVERRIDE(float, Rule, get_confidence, );
    }

    std::vector<std::string> get_base() override {
        PYBIND11_OVERRIDE(std::vector<std::string>, Rule, get_base, );
    }

    std::vector<std::string> get_add() override {
        PYBIND11_OVERRIDE(std::vector<std::string>, Rule, get_add, );
    }
};


class TrampolineLattice : public Lattice {
public:
    /* Inherit the constructors */
    using Lattice::Lattice;

    /* Trampoline (need one for each virtual function) */
    std::string __repr__() override {
        PYBIND11_OVERRIDE(std::string, Lattice, __repr__, );
    }

    Concept &get_concept(int i) override {
        PYBIND11_OVERRIDE(Concept &, Lattice, get_concept, i);
    };

    void add_intent(std::string object_name, const std::vector<int> &intent) override {
        PYBIND11_OVERRIDE(void, Lattice, add_intent, object_name, intent);
    };

    void add_intent_in_bulk(std::vector<std::string> object_names, const std::vector<int> &intent) override {
        PYBIND11_OVERRIDE(void, Lattice, add_intent_in_bulk, object_names, intent);
    };

    int add_attribute(std::string attr_name) override {
        PYBIND11_OVERRIDE(int, Lattice, add_attribute, attr_name);
    };

    int add_object(std::string object_name) override {
        PYBIND11_OVERRIDE(int, Lattice, add_object, object_name);
    };

    void add_pair(std::string object_name, std::string attr_name) override {
        PYBIND11_OVERRIDE(void, Lattice, add_pair, object_name, attr_name);
    };


    void delete_instance(std::string object_name) override {
        PYBIND11_OVERRIDE(void, Lattice, delete_instance, object_name);
    };

    Lattice *merge_concepts(Lattice &l) override {
        PYBIND11_OVERRIDE(Lattice *, Lattice, merge_concepts, l);
    };

    std::vector<Rule *> get_association_rules(std::vector<std::string> containing,
                                              float min_support,
                                              float min_confidence) override {
        PYBIND11_OVERRIDE(std::vector<Rule *>, Lattice, get_association_rules, containing, min_support, min_confidence);
    }
};


std::vector<Concept *> api_inclose(std::vector<std::string> &G,
                                   std::vector<std::string> &M,
                                   std::vector<std::vector<int>> &I) {
    auto ctx = Context(G, M, I);
    return inclose(ctx);
}


PYBIND11_MODULE(fca_algorithms_cpp, mod) {
    mod.def("inclose", &api_inclose, "C++ implementation of the inclose algorithm.");

    py::class_<Context, TrampolineContext /* <--- trampoline*/>(mod, "ContextCpp")
        .def(py::init<std::vector<std::string> &, std::vector<std::string> &, std::vector<std::vector<int>> &>())
        .def("get_concepts", &Context::get_concepts)
        .def_readwrite("G", &Context::G)
        .def_readwrite("M", &Context::M)
        .def_readwrite("I", &Context::I)
        .def_readwrite("O", &Context::G)
        .def_readwrite("A", &Context::M)
        .def("__repr__", &Context::__repr__)
        .def(py::pickle(
            [](const Context &ctx) { // __getstate__
                /* Return a tuple that fully encodes the state of the object */
                return py::make_tuple(ctx.G, ctx.M, ctx.I);
            },
            [](py::tuple t) { // __setstate__
                if (t.size() != 3)
                    throw std::runtime_error("Invalid state!");

                /* Create a new C++ instance */
                std::vector<std::string> G = t[0].cast<std::vector<std::string>>();
                std::vector<std::string> M = t[1].cast<std::vector<std::string>>();
                std::vector<std::vector<int>> I = t[2].cast<std::vector<std::vector<int>>>();
                Context ctx(G, M, I);
                return ctx;
            }));


    py::class_<Concept, TrampolineConcept /* <--- trampoline*/>(mod, "ConceptCpp")
        //.def(py::init<>())
        .def(py::init<Context &, std::vector<int> &, std::vector<int> &>())
        .def("__repr__", &Concept::__repr__)
        .def_readonly("children", &Concept::children)
        .def_readonly("parents", &Concept::parents)
        .def_readwrite("X", &Concept::X)
        .def_readwrite("Y", &Concept::Y)
        .def_readwrite("O", &Concept::X)
        .def_readwrite("A", &Concept::Y)
        .def("hr_X", &Concept::hr_X)
        .def("hr_Y", &Concept::hr_Y)
        .def("hr_O", &Concept::hr_X)
        .def("hr_A", &Concept::hr_Y)
        .def("add_child", &Concept::add_child)
        .def("remove_child", &Concept::remove_child)
        .def("to_tuple", &Concept::to_tuple)
        .def(py::pickle(
            [](const Concept &c) { // __getstate__
                /* Return a tuple that fully encodes the state of the object */
                return py::make_tuple(c.ctx, c.X, c.Y);
            },
            [](py::tuple t) { // __setstate__
                if (t.size() != 3)
                    throw std::runtime_error("Invalid state!");

                /* Create a new C++ instance */
                Context ctx = t[0].cast<Context>();
                std::vector<int> X = t[1].cast<std::vector<int>>();
                std::vector<int> Y = t[2].cast<std::vector<int>>();
                Context *new_ctx = new Context(ctx.G, ctx.M, ctx.I);
                Concept c(*new_ctx, X, Y);
                return c;
            }));


    py::class_<Rule, TrampolineRule /* <--- trampoline*/>(mod, "Rule")
        //.def(py::init<>())
        .def(py::init<float, float, std::vector<std::string>, std::vector<std::string>>())
        .def("get_support", &Rule::get_support)
        .def("get_confidence", &Rule::get_confidence)
        .def("get_base", &Rule::get_base)
        .def("get_add", &Rule::get_add)
        .def("__repr__", &Rule::__repr__)
        .def(py::pickle(
            [](Rule &r) { // __getstate__
                /* Return a tuple that fully encodes the state of the object */
                return py::make_tuple(r.get_support(), r.get_confidence(), r.get_base(), r.get_add());
            },
            [](py::tuple t) { // __setstate__
                if (t.size() != 4)
                    throw std::runtime_error("Invalid state!");

                /* Create a new C++ instance */
                float support = t[0].cast<float>();
                float confidence = t[1].cast<float>();
                std::vector<std::string> base = t[2].cast<std::vector<std::string>>();
                std::vector<std::string> add = t[3].cast<std::vector<std::string>>();
                Rule r(support, confidence, base, add);
                return r;
            }));


    py::class_<Lattice, TrampolineLattice /* <--- trampoline*/>(mod, "LatticeCpp")
        //.def(py::init<>())
        .def(py::init<Context &>())
        .def_static("copy", &Lattice::copy)
        .def_readonly("concepts", &Lattice::_concepts)
        .def_readwrite("ctx", &Lattice::ctx)
        .def("get_top", &Lattice::get_top)
        .def("get_bottom", &Lattice::get_bottom)
        .def("__repr__", &Lattice::__repr__)
        .def("get_concept", &Lattice::get_concept)
        .def("add_intent", &Lattice::add_intent)
        .def("add_intent_in_bulk", &Lattice::add_intent_in_bulk)
        .def("add_attribute", &Lattice::add_attribute)
        .def("add_object", &Lattice::add_object)
        .def("add_pair", &Lattice::add_pair)
        .def("delete_instance", &Lattice::delete_instance)
        .def("merge_concepts", &Lattice::merge_concepts)
        .def("get_association_rules",
             &Lattice::get_association_rules,
             "A function that returns the list of association rules",
             py::arg("containing"),
             py::arg("min_support") = 0.2,
             py::arg("min_confidence") = 1.0)
        .def(py::pickle(
            [](Lattice &l) { // __getstate__
                /* Return a tuple that fully encodes the state of the object */
                std::vector<pybind11::tuple> all_concepts;
                std::map<Concept *, int> concept_by_idx;
                int bottom_idx = -1, top_idx = -1;
                for (int i = 0; i < l._concepts.size(); ++i) {
                    auto c = l._concepts[i];
                    if (c == &l.get_bottom())
                        bottom_idx = i;
                    if (c == &l.get_top())
                        top_idx = i;
                    all_concepts.push_back(py::make_tuple(c->X, c->Y));
                    concept_by_idx.insert({c, i});
                }
                std::vector<std::vector<int>> children;
                std::vector<std::vector<int>> parents;
                for (int i = 0; i < l._concepts.size(); ++i) {
                    auto c = l._concepts[i];
                    children.push_back(std::vector<int>());
                    parents.push_back(std::vector<int>());
                    for (auto child : c->children)
                        children.back().push_back(concept_by_idx.at(child));
                    for (auto parent : c->parents)
                        parents.back().push_back(concept_by_idx.at(parent));
                }
                return py::make_tuple(l.ctx, bottom_idx, top_idx, all_concepts, children, parents);
            },
            [](py::tuple t) { // __setstate__
                if (t.size() != 6)
                    throw std::runtime_error("Invalid state!");

                /* Create a new C++ instance */
                Context ctx = t[0].cast<Context>();
                int bottom_idx = t[1].cast<int>();
                int top_idx = t[2].cast<int>();
                std::vector<std::pair<std::vector<int>, std::vector<int>>> all_concepts =
                    t[3].cast<std::vector<std::pair<std::vector<int>, std::vector<int>>>>();
                std::vector<std::vector<int>> children = t[4].cast<std::vector<std::vector<int>>>();
                std::vector<std::vector<int>> parents = t[5].cast<std::vector<std::vector<int>>>();
                Context dummyctx;
                Lattice l(dummyctx);
                Context *new_ctx = new Context(ctx.G, ctx.M, ctx.I);
                l.ctx = new_ctx;

                l._concepts.clear();
                for (auto p : all_concepts)
                    l._concepts.push_back(new Concept(*l.ctx, p.first, p.second));
                l.set_top(l._concepts[top_idx]);
                l.set_bottom(l._concepts[bottom_idx]);
                for (int i = 0; i < children.size(); ++i) {
                    auto children_of_i = children[i];
                    for (int child_idx : children_of_i)
                        l._concepts[i]->children.push_back(l._concepts[child_idx]);
                }
                for (int i = 0; i < parents.size(); ++i) {
                    auto parents_of_i = parents[i];
                    for (int parent_idx : parents_of_i)
                        l._concepts[i]->parents.push_back(l._concepts[parent_idx]);
                }
                return l;
            }));
}
