#include "add_pair.h"
#include "utils.h"

using namespace std;


int add_attribute(vector<int> &old_intent, int m, vector<Concept *> &concepts) {
    // std::vector<std::vector<int>> common_attributes = get_common_attributes(old_intent, l);
    // int c_idx = get_maximal_I_concept(old_intent, 0, l, common_attributes);
    // Concept* c_so_far = get<0>(l[c_idx]);
    // if (c_so_far->X.size() > 1){ // c_so_far won't change because it has more objects than the one that is changing.
    //     vector<int> new_intent(c_so_far->Y);
    //     add_ordered_if_not_present(new_intent, m);
    //     c_idx = get_maximal_I_concept(new_intent, 0, l, common_attributes);
    //     c_so_far = get<0>(l[c_idx]);
    //     if(c_so_far->Y.size() != new_intent.size()){  // There's no concept (A, B) with exactly B = old_intent \cup {m}
    //         c_so_far = new Concept(c_so_far->ctx, c_so_far->X, new_intent);
    //         l.push_back(make_tuple(c_so_far, vector<int>(), vector<int>()));  // it should be correctly inserted with the same connections as c_idx
    //         c_idx = l.size() - 1;
    //     }
    // } else {
    //     add_ordered_if_not_present(c_so_far->Y, m);
    // }
    return -1;
}

int add_pair(int g, int m, vector<Concept *> &concepts, _Context &ctx) {
    // Precondition: Both `g` and `m` are already added to l i.e.,  |ctx.G| < g ^ |ctx.M| < m;
    // vector<int> old_intent;
    // for (int i = 0; i < ctx.I[g].size(); ++i){
    //     if(ctx.I[g][i])
    //         old_intent.push_back(i);
    // }
    // int c_idx = add_attribute(old_intent, m, l);
    // Concept* c = get<0>(l[c_idx]);
    // add_ordered_if_not_present(c->X, g);
    return -1;
}