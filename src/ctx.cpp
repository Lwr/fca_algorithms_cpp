#include "ctx.h"
#include <iostream>

Context::Context(std::vector<std::string> &p_G,
                 std::vector<std::string> &p_M,
                 std::vector<std::vector<int>> &p_I,
                 FCASolver *p_solver)
    : _Context(p_G, p_M, p_I), solver(p_solver) {
}

Context::Context(std::vector<std::string> &p_G, std::vector<std::string> &p_M, std::vector<std::vector<int>> &p_I)
    : _Context(p_G, p_M, p_I), solver(new IncloseSolver()) {
}


Context::Context() {
}

Context::~Context() {
    // if (solver != nullptr)
    //     delete solver;
}

void Context::initialize(FCASolver &p_solver) {
}

std::vector<Concept *> Context::get_concepts() {
    return solver->get_concepts(*this);
}