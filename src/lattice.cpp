#include "lattice.h"
#include "addintent.h"
#include "delete_instance.h"
#include "intents_dict.h"
#include "utils.h"
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>

void insert_ordered(int g, std::vector<int> &extent) {
    auto low = std::lower_bound(extent.begin(), extent.end(), g);
    if (low != extent.end() && *low == g)
        return;

    extent.insert(low, g);
}


Concept *dfs_to_add_g_to_extent_and_concepts_above(int g, Concept *c, std::set<Concept *> &visited) {
    visited.insert(c);
    insert_ordered(g, c->X);
    Concept *top;
    for (auto neighbour : c->parents)
        if (visited.find(neighbour) == visited.end())
            top = dfs_to_add_g_to_extent_and_concepts_above(g, neighbour, visited);

    if (c->parents.size() == 0)
        return c;
    else
        return top;
}

Concept *dfs_to_add_gs_to_extent_and_concepts_above(std::vector<int> &gs, Concept *c, std::set<Concept *> &visited) {
    visited.insert(c);
    for (int g : gs)
        insert_ordered(g, c->X);
    Concept *top;
    for (auto neighbour : c->parents)
        if (visited.find(neighbour) == visited.end())
            top = dfs_to_add_gs_to_extent_and_concepts_above(gs, neighbour, visited);

    if (c->parents.size() == 0)
        return c;
    else
        return top;
}


Concept *add_g_to_extent_and_concepts_above(int g, Concept *c) {
    std::set<Concept *> visited;
    return dfs_to_add_g_to_extent_and_concepts_above(g, c, visited);
}


Concept *add_gs_to_extent_and_concepts_above(std::vector<int> &gs, Concept *c) {
    std::set<Concept *> visited;
    return dfs_to_add_gs_to_extent_and_concepts_above(gs, c, visited);
}


void Lattice::add_object_with_intent(std::string object_name, const std::vector<int> &intent) {
    ctx->G.push_back(object_name);
    ctx->I.push_back(std::vector<int>(ctx->M.size(), 0));
    for (int elem : intent) {
        ctx->I.back()[elem] = 1;
    }
}


int Lattice::get_object_idx(const std::string &object_name) {
    auto it = std::find(ctx->G.begin(), ctx->G.end(), object_name);
    if (it != ctx->G.end()) {
        int index = it - ctx->G.begin();
        return index;
    }
    return -1;
}


int Lattice::get_attribute_idx(const std::string &attr_name) {
    auto it = std::find(ctx->M.begin(), ctx->M.end(), attr_name);
    if (it != ctx->M.end()) {
        int index = it - ctx->M.begin();
        return index;
    }
    return -1;
}

std::vector<int> Lattice::get_intent(int g) {
    std::vector<int> intent;
    for (int m = 0; m < ctx->M.size(); ++m)
        if (ctx->I[g][m])
            intent.push_back(m);
    return intent;
}


std::vector<int> Lattice::get_extent(int m) {
    std::vector<int> extent;
    for (int g = 0; g < ctx->G.size(); ++g)
        if (ctx->I[g][m])
            extent.push_back(g);
    return extent;
}


void Lattice::update_bottom(const std::string &attr_name) {
    int m = ctx->M.size();
    ctx->M.push_back(attr_name);
    for (int g = 0; g < ctx->G.size(); ++g)
        ctx->I[g].push_back(0);
    if (_bottom->X.size() == 0) {
        _bottom->Y.push_back(m); // I know m is greater than all elements in _bottom->Y
    } else {
        std::vector<int> new_intent(_bottom->Y);
        new_intent.push_back(m);
        Concept *new_bottom = new Concept(*ctx, {}, new_intent);
        set_link(_bottom, new_bottom);
        _bottom = new_bottom;
        _concepts.push_back(new_bottom);
    }
}

Lattice::Lattice(Context &context) : ctx(&context) {
    std::vector<int> bottom_intent;
    for (int i = 0; i < context.M.size(); ++i)
        bottom_intent.push_back(i);
    auto bottom_concept = new Concept(context, {}, bottom_intent);
    _concepts.push_back(bottom_concept);
    _bottom = bottom_concept;
    _top = bottom_concept;
    for (int g = 0; g < context.G.size(); ++g) {
        std::vector<int> intent;
        for (int m = 0; m < context.M.size(); ++m) {
            if (context.I[g][m])
                intent.push_back(m);
        }
        Concept *concept_idx = AddIntentImpl::addintent(intent, _bottom, *this, *ctx);
        _top = add_g_to_extent_and_concepts_above(g, concept_idx);
    }
}

Lattice *Lattice::copy(Lattice &l) {
    auto dummy_ctx = new Context();
    Lattice *lattice = new Lattice(*dummy_ctx);
    lattice->_concepts.clear();
    for (auto g : l.ctx->G)
        lattice->ctx->G.push_back(g);
    for (auto m : l.ctx->M)
        lattice->ctx->M.push_back(m);
    for (auto v : l.ctx->I)
        lattice->ctx->I.push_back(std::vector<int>(v));

    std::vector<std::vector<int>> children;
    std::vector<std::vector<int>> parents;
    std::map<Concept *, int> idx_by_concept;
    for (int i = 0; i < l._concepts.size(); ++i)
        idx_by_concept.insert({l._concepts[i], i});


    for (int i = 0; i < l._concepts.size(); ++i) {
        children.push_back(std::vector<int>());
        parents.push_back(std::vector<int>());
        auto c = l._concepts[i];
        for (auto child : c->children)
            children[i].push_back((*idx_by_concept.find(child)).second);
        for (auto parent : c->parents)
            parents[i].push_back((*idx_by_concept.find(parent)).second);
    }

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        lattice->_concepts.push_back(new Concept(*lattice->ctx, c->X, c->Y));
        if (lattice->_concepts[i]->X.size() == lattice->ctx->G.size())
            lattice->_top = lattice->_concepts[i];

        if (lattice->_concepts[i]->Y.size() == lattice->ctx->M.size())
            lattice->_bottom = lattice->_concepts[i];
    }
    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = lattice->_concepts[i];
        for (int c_idx : children[i])
            c->children.push_back(lattice->_concepts[c_idx]);
        for (int c_idx : parents[i])
            c->parents.push_back(lattice->_concepts[c_idx]);
    }
    return lattice;
}

void Lattice::initialize() {
    ctx = new Context();
}

Lattice::~Lattice() {
}

void Lattice::set_bottom(Concept *c) {
    _bottom = c;
}

void Lattice::set_top(Concept *c) {
    _top = c;
}

Concept &Lattice::get_concept(int i) {
    if (_concepts.size() <= i || i < 0)
        throw std::invalid_argument("Argument `i` must be positive and lesser than the amount of concepts");

    return *_concepts[i];
}

Concept &Lattice::get_top() {
    return *_top;
}

Concept &Lattice::get_bottom() {
    return *_bottom;
}


void Lattice::add_intent(std::string object_name, const std::vector<int> &intent) {
    int g = ctx->G.size();
    add_object_with_intent(object_name, intent);
    Concept *concept_idx = AddIntentImpl::addintent(intent, _bottom, *this, *ctx);
    _top = add_g_to_extent_and_concepts_above(g, concept_idx);
}


void Lattice::add_intent_in_bulk(std::vector<std::string> object_names, const std::vector<int> &intent) {
    int g = ctx->G.size();
    for (auto object_name : object_names)
        add_object_with_intent(object_name, intent);
    Concept *concept_idx = AddIntentImpl::addintent(intent, _bottom, *this, *ctx);

    std::vector<int> object_indexes;
    for (int i = 0; i < object_names.size(); ++i) {
        object_indexes.push_back(g + i);
    }
    _top = add_gs_to_extent_and_concepts_above(object_indexes, concept_idx);
}


void Lattice::delete_instance(std::string object_name) {
    int g = get_object_idx(object_name);
    DeleteInstanceImpl::deleteinstance(g, *this, *ctx);
}


int Lattice::add_attribute(std::string attr_name) {
    int m = get_attribute_idx(attr_name);
    if (m == -1) {
        update_bottom(attr_name);
        m = ctx->M.size() - 1;
    }
    return m;
}

int Lattice::add_object(std::string object_name) {
    int g = get_object_idx(object_name);
    if (g == -1) {
        add_intent(object_name, {});
        g = get_object_idx(object_name);
    }
    return g;
}


void Lattice::add_pair(std::string object_name, std::string attr_name) {
    int g = get_object_idx(object_name);
    int m = get_attribute_idx(attr_name);
    if (m == -1) {
        update_bottom(attr_name);
        m = ctx->M.size() - 1;
    }
    std::vector<int> intent = {m};
    if (g != -1) {
        intent = get_intent(g);
        DeleteInstanceImpl::deleteinstance(g, *this, *ctx);
        insert_ordered(m, intent);
    }
    add_intent(object_name, intent);
}


std::string Lattice::__repr__() {
    std::string s = "Lattice: { ";
    for (int i = 0; i < _concepts.size(); ++i) {
        s += get_concept(i).__repr__();
        if (i < _concepts.size() - 1)
            s += ", ";
    }
    s += "}\n";
    return s;
}


void initialize_dict(std::unordered_map<std::string, int> &dict, std::vector<std::string> &x, int offset) {
    for (int i = 0; i < x.size(); ++i)
        dict.insert({x[i], offset + i});
}

void incidence_union(Context &ctx_1,
                     Context &ctx_2,
                     std::unordered_map<std::string, int> &a1,
                     std::unordered_map<std::string, int> &a2,
                     std::vector<std::vector<int>> &I) {
    I.clear();
    for (auto v : ctx_1.I)
        I.push_back(std::vector<int>(v));

    int new_attributes = 0;
    for (int j = 0; j < ctx_2.M.size(); ++j) {
        auto a = ctx_2.M[j];
        if (!unordered_map_contains(a1, a)) {
            new_attributes++;
            a1.insert({a, a1.size()});
            for (int i = 0; i < ctx_1.G.size(); ++i)
                I[i].push_back(0);
        }
    }

    for (int i = 0; i < ctx_2.G.size(); ++i) {
        auto o = ctx_2.G[i];
        int offset_index = I.size();
        I.push_back(std::vector<int>(ctx_1.M.size() + new_attributes, 0));
        for (auto &it : a1) {
            int j = it.second;
            auto m = it.first;
            if (unordered_map_contains(a2, m))
                I[offset_index][j] = ctx_2.I[i][a2.at(m)];
        }
    }
}


Lattice *Lattice::merge_concepts(Lattice &l) {
    // Returns a Lattice with a list of concepts
    // resulting from merging the two lattices.
    // However, it doesn't create the connections
    // between them.
    // Precondition:
    // objects (G) in both lattices are disjoint.
    // i.e., ctx->G U l.ctx->G = empty
    std::unordered_map<std::string, int> o1, a1, o2, a2, k_s_O, k_s_A;
    std::unordered_map<int, int> from_a1_idx_to_a2_idx;
    initialize_dict(o1, ctx->G, 0);           // O(|G_1|)
    initialize_dict(o1, l.ctx->G, o1.size()); // O(|G_2|)
    initialize_dict(a1, ctx->M, 0);           // O(|M_1|)
    initialize_dict(o2, l.ctx->G, 0);         // O(|G_2|)
    initialize_dict(a2, l.ctx->M, 0);         // O(|M_2|)

    // This ones could be done like this because G's are disjoint
    initialize_dict(k_s_O, ctx->G, 0);              // O(|G_1|)
    initialize_dict(k_s_O, l.ctx->G, k_s_O.size()); // O(|G_2|)

    initialize_dict(k_s_A, l.ctx->M, 0);            // O(|M_2|)
    for (int i = 0; i < ctx->M.size(); ++i)         // O(|M_1|)
        if (!unordered_map_contains(k_s_A, ctx->M[i]))
            k_s_A.insert({ctx->M[i], i});

    std::vector<std::string> new_G(ctx->G);
    for (auto elem : l.ctx->G)
        new_G.push_back(elem);

    std::vector<std::string> new_M(ctx->M);
    for (auto elem : l.ctx->M)
        if (!unordered_map_contains(a1, elem))
            new_M.push_back(elem);

    std::vector<std::vector<int>> k_s_I;


    incidence_union(*ctx, *l.ctx, a1, a2, k_s_I); // O((|G_1| + |G_2|) * |M_1| + |M_2|)

    Context *new_ctx = new Context();
    Lattice *lattice_res = new Lattice(*new_ctx);
    lattice_res->_concepts.clear();
    new_ctx->G = new_G;
    new_ctx->M = new_M;
    new_ctx->I = k_s_I;

    std::vector<std::pair<std::vector<int>, std::vector<int>>> l2_intents;
    IntentDict all_intents;

    for (int i = 0; i < l._concepts.size(); ++i) { // O(|L_2|)
        auto c = l._concepts[i];
        std::vector<int> a_offset_index;
        auto elems = c->hr_Y();
        for (int j = 0; j < elems.size(); j++) { // O(|M_2|) ooor a bit more sharp O(max(|g'|))
            auto elem = elems[j];
            if (!unordered_map_contains(a1, elem))
                a1.insert({elem, a1.size()});
            if (!unordered_map_contains_int(from_a1_idx_to_a2_idx, a1.at(elem)))
                from_a1_idx_to_a2_idx.insert({a1.at(elem), c->Y[j]});
            a_offset_index.push_back(a1.at(elem));
        }
        std::vector<int> o_offset_index;
        auto objects = c->hr_X();
        for (int i = 0; i < objects.size(); ++i) { // O(|G_2|) ooor a bit more sharp O(max(|m'|))
            auto object = objects[i];
            if (!unordered_map_contains(o1, object))
                o1.insert({object, o1.size()});
            o_offset_index.push_back(o1.at(object));
        }
        std::sort(o_offset_index.begin(),
                  o_offset_index.end()); // O(|G_2|log(|G_2|)) ooor a bit more sharp O(max(|m'|) log(max(|m'|)))
        std::sort(a_offset_index.begin(),
                  a_offset_index.end()); // O(|M_2|log(|M_2|)) ooor a bit more sharp O(max(|g'|) log(max(|g'|)))

        l2_intents.push_back({o_offset_index, a_offset_index});

        Concept *maximal_concept = get_maximal_I_concept(a_offset_index, _bottom);              // O(|G_1|^2|M_1|)
        if (intersection(maximal_concept->Y, a_offset_index).size() == a_offset_index.size()) { // O(|M_1|log(|M_1|))
            for (auto elem : maximal_concept->X)      // by adding more objects, the relationships can change
                insert_ordered(elem, o_offset_index); // O(|G_1|log(max(|g2'|)))
        }

        all_intents.insert(a_offset_index, o_offset_index);
    } // in total, this loop takes O(|L_2|max(|G_1| |M_1|, |G_2|log(|G_2|), O(|M_2|log(|M_2|)) or its more sharp version
    // O(|L_2| max(|G_1| max(|g'|), max(|m_2'|)log(max(|m_2'|), max(g_2') log(max(|g_2'|))
    // in short, if we take the usual case where G is large and M not so much. We can say it's complexity is O(|L_2||G_1| max(|g_1'|))


    for (auto c : _concepts) { // O(|L_1|)
        auto intent = c->Y;
        std::vector<int> extent(c->X);
        auto intent_in_all_intents =
            all_intents.get(intent); // O(|M_1|) or O (max(|g_1'|)) (we use a trie for the intents)
        if (!intent_in_all_intents.first) {
            std::vector<int> current_l2_intent;
            bool consider = true;
            for (auto elem : intent) {
                if (!unordered_map_contains_int(from_a1_idx_to_a2_idx, elem)) {
                    consider = false;
                    break;
                }
                current_l2_intent.push_back(from_a1_idx_to_a2_idx.at(elem));
            }
            if (consider) {
                auto concept_to_consider = get_maximal_I_concept(current_l2_intent, l._bottom); // O(|G_2||M_2|)
                if (intersection(concept_to_consider->Y, current_l2_intent).size() == current_l2_intent.size()) {
                    for (auto i : concept_to_consider->X)
                        insert_ordered(ctx->G.size() + i, extent);
                }
            }
            all_intents.insert(intent, extent);
        } // In total, this loop takes O(|L_1||G_2||M_2|) or O(|L_1||G_2| max(|g'|))
    }

    std::list<std::pair<std::vector<int>, std::vector<int>>> intents_and_extents;
    all_intents.all_elems(intents_and_extents);
    for (auto it = intents_and_extents.begin(); it != intents_and_extents.end(); ++it) { // O (|all_intents|)
        auto intent = (*it).first;
        auto extent = (*it).second;
        auto c = new Concept(*lattice_res->ctx, extent, intent);

        if (c->X.size() != 0 && c->Y.size() != 0 && c->X.size() != lattice_res->ctx->G.size() &&
            c->Y.size() != lattice_res->ctx->M.size()) {
            lattice_res->_concepts.push_back(c);
        }
    }

    for (int idx = 0; idx < _concepts.size(); ++idx) {          // O (|L_1|)
        auto c1 = _concepts[idx];
        for (int idx2 = 0; idx2 < l._concepts.size(); ++idx2) { // O (|L_2|)
            auto c2 = l._concepts[idx2];
            if (c1->X.size() == 0 || c2->X.size() == 0 || c1->Y.size() == 0 || c2->Y.size() == 0)
                continue;

            auto a_offset_index = l2_intents[idx2].second;
            auto intersect = intersection(c1->Y, a_offset_index);
            auto maybe_extent = all_intents.get(intersect); // O(|M_1| \cap |M_2|)
            bool is_in_all_concepts = maybe_extent.first;
            if (intersect.size() != 0 && !is_in_all_concepts) {
                auto concept_to_consider = get_maximal_I_concept(intersect, _bottom); // O(|G_1||M_1|)
                std::vector<int> objects_1;
                if (intersection(concept_to_consider->Y, intersect).size() == intersect.size()) {
                    for (auto g : concept_to_consider->X)
                        objects_1.push_back(g);
                }

                std::vector<int> transformed_intersect(intersect);
                for (int i = 0; i < transformed_intersect.size(); ++i)
                    transformed_intersect[i] = from_a1_idx_to_a2_idx.at(intersect[i]);

                auto concept_to_consider_2 = get_maximal_I_concept(transformed_intersect, l._bottom); // O(|G_2||M_2|)
                std::vector<int> objects_2;
                if (intersection(concept_to_consider_2->Y, transformed_intersect).size() ==
                    transformed_intersect.size()) {
                    for (auto g : concept_to_consider_2->X)
                        objects_2.push_back(ctx->G.size() + g);
                }
                for (auto x : objects_2)
                    insert_ordered(x, objects_1);
                auto new_concept = new Concept(*lattice_res->ctx, objects_1, intersect);
                if (new_concept->X.size() != lattice_res->ctx->G.size() &&
                    new_concept->Y.size() != lattice_res->ctx->M.size()) {
                    if (new_concept->X.size() == 0) {
                        for (auto g : concept_to_consider_2->X)
                            objects_2.push_back(ctx->G.size() + g);
                    }
                    lattice_res->_concepts.push_back(new_concept);
                    all_intents.insert(new_concept->Y, new_concept->X);
                }
            }
        }
    } // Total cost of the loop O(|L_1||L_2|max(|G_1||M_1|, |G_2||M_2|))

    std::vector<int> all_g(new_G.size());
    std::vector<int> objects_with_all_attributes;
    for (int i = 0; i < new_G.size(); ++i) {
        all_g[i] = i;
        bool has_all_attributes = true;
        for (int j = 0; j < new_M.size(); ++j) {
            if (k_s_I[i][j] != 1) {
                has_all_attributes = false;
                break;
            }
        }
        if (has_all_attributes) {
            objects_with_all_attributes.push_back(i);
        }
    } // O(|G_m M_m|)
    std::vector<int> all_m(new_M.size());
    std::vector<int> attr_with_all_objects;
    for (int j = 0; j < new_M.size(); ++j) {
        all_m[j] = j;
        bool has_all_objects = true;
        for (int i = 0; i < new_G.size(); ++i) {
            if (k_s_I[i][j] != 1) {
                has_all_objects = false;
                break;
            }
        }
        if (has_all_objects) {
            attr_with_all_objects.push_back(j);
        }
    }
    lattice_res->_top = new Concept(*lattice_res->ctx, objects_with_all_attributes, all_m);
    lattice_res->_bottom = new Concept(*lattice_res->ctx, all_g, attr_with_all_objects);
    lattice_res->_concepts.push_back(lattice_res->_bottom);
    lattice_res->_concepts.push_back(lattice_res->_top);

    return lattice_res; // Total cost of the whole function O(|L_1||L_2| max(|G_1||M_1|, |G_2||M_2|))
                        // or more accurately O(|L_1||L_2| (|G_1| max(|g'1|), |G_2| max(|g'2|)))
}


bool are_eq(int idx, std::vector<int> &v1, std::vector<int> &v2) {
    bool are_equal = true;
    for (int i = 0; i < idx; ++i)
        are_equal = are_equal && v1[i] == v2[i];
    return are_equal;
}


std::list<std::vector<int>> apriori_gen(std::list<std::vector<int>> &h_m, int consequent_size) {
    std::list<std::vector<int>> res;
    for (auto it = h_m.begin(); it != h_m.end(); ++it) {
        auto v1 = *it;
        std::list<std::vector<int>>::iterator new_it(it);
        new_it++;
        while (new_it != h_m.end() && are_eq(consequent_size - 1, *it, *new_it)) {
            auto v2 = *new_it;
            std::vector<int> new_consequent(v1);
            new_consequent.push_back(v2.back());
            res.push_back(new_consequent);
            new_it++;
        }
    }
    return res;
}


void Lattice::_get_association_rules(Concept *c,
                                     std::list<std::vector<int>> consequents,
                                     int consequent_size,
                                     std::vector<int> containing,
                                     float min_support,
                                     float min_confidence,
                                     std::vector<Rule *> &rules) {

    float concept_support = float(c->X.size()) / float(ctx->G.size());
    if (concept_support < min_support)
        return;

    int intent_size = c->Y.size();
    if (intent_size > consequent_size + 1) {
        std::list<std::vector<int>> h_m_plus_one = apriori_gen(consequents, consequent_size);
        for (auto it = h_m_plus_one.begin(); it != h_m_plus_one.end();) {
            auto h_m = *it;

            std::vector<int> intent_to_consider;
            for (auto x : c->Y)
                if (is_there(h_m, x))
                    intent_to_consider.push_back(x); // this should be sorted
            auto considering_c = get_maximal_I_concept(intent_to_consider, c);
            float against_support = (float(considering_c->X.size()) / float(ctx->G.size()));
            float confidence =
                against_support != 0 ? concept_support / against_support : (concept_support == 0 ? 1 : -1);
            if (confidence >= min_confidence && is_included(containing, intent_to_consider)) {
                std::vector<std::string> consequent, antecedent;
                for (auto i : h_m)
                    consequent.push_back(ctx->M[i]);

                for (auto i : intent_to_consider)
                    antecedent.push_back(ctx->M[i]);


                rules.push_back(new Rule(concept_support, confidence, antecedent, consequent));
                it++;
            } else {
                it = h_m_plus_one.erase(it);
            }
        }
        _get_association_rules(c, h_m_plus_one, consequent_size + 1, containing, min_support, min_confidence, rules);
    }
}


void Lattice::_get_association_rules(Concept *c,
                                     std::vector<int> containing,
                                     float min_support,
                                     float min_confidence,
                                     std::vector<Rule *> &rules) {

    float concept_support = float(c->X.size()) / float(ctx->G.size());
    if (concept_support < min_support)
        return;

    std::list<std::vector<int>> h1_list;
    for (auto x : c->Y) {
        h1_list.push_back({x});
    }
    std::vector<int> intent_to_consider;
    for (auto it = h1_list.begin(); it != h1_list.end();) {
        auto h1 = *it;
        intent_to_consider.clear();
        for (auto x : c->Y)
            if (x != h1[0])
                intent_to_consider.push_back(x); // this should be sorted

        auto considering_c = get_maximal_I_concept(intent_to_consider, c);
        float against_support = (float(considering_c->X.size()) / float(ctx->G.size()));
        float confidence = against_support != 0 ? concept_support / against_support : (concept_support == 0 ? 1 : -1);
        if (confidence >= min_confidence && is_included(containing, intent_to_consider)) {
            std::vector<std::string> consequent, antecedent;
            for (auto i : h1)
                consequent.push_back(ctx->M[i]);

            for (auto i : intent_to_consider)
                antecedent.push_back(ctx->M[i]);
            rules.push_back(new Rule(concept_support, confidence, antecedent, consequent));
            ++it;
        } else {
            it = h1_list.erase(it);
        }
    }
    _get_association_rules(c, h1_list, 1, containing, min_support, min_confidence, rules);
}


std::vector<Rule *> Lattice::get_association_rules(std::vector<std::string> containing,
                                                   float min_support,
                                                   float min_confidence) {
    std::vector<int> containing_set;
    for (auto m : containing)
        containing_set.push_back(get_attribute_idx(m));

    std::sort(containing_set.begin(), containing_set.end());
    std::vector<Rule *> rules;
    // starting from this one, all childrens have to be considered
    auto c = get_maximal_I_concept(containing_set, _bottom);
    std::unordered_set<Concept *> visited;
    std::vector<Concept *> to_process = {c};
    while (!to_process.empty()) {
        auto current_c = to_process.back();
        to_process.pop_back();
        visited.insert(current_c);
        _get_association_rules(current_c, containing_set, min_support, min_confidence, rules);
        for (auto child : current_c->children)
            if (visited.find(child) == visited.end())
                to_process.push_back(child);
    }
    return rules;
}
