#!/bin/bash
echo "Deleting dist/ folder"
rm -rf dist

echo "Build project"
python3 -m build

echo "Upload project"
python -m twine upload --repository pypi wheelhouse/*.whl --verbose --skip-existing -u $PIP_USERNAME -p $PIP_PASSWORD
