#ifndef FCA_ALGORITHMS_ADD_PAIR_H
#define FCA_ALGORITHMS_ADD_PAIR_H


#include <base_models.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <vector>


int add_pair(int g, int m, std::vector<Concept *> &concepts, _Context &ctx);


#endif //FCA_ALGORITHMS_ADD_PAIR_H
