#include "base_models.h"
#include <list>
#include <unordered_map>
#include <vector>

class IntentDict {
private:
    std::list<std::vector<int>> extents_by_intent;
    std::unordered_map<int, IntentDict *> intent_dicts;
    void insert(std::vector<int> &intent, std::vector<int> &c, int current_index);
    std::pair<bool, std::vector<int> &> get(std::vector<int> &intent, int current_index);
    void all_elems(std::list<std::pair<std::vector<int>, std::vector<int>>> &to_fill, std::vector<int> intent_so_far);

public:
    IntentDict();
    ~IntentDict();
    void insert(std::vector<int> &intent, std::vector<int> &extent);
    std::pair<bool, std::vector<int> &> get(std::vector<int> &intent);
    void all_elems(std::list<std::pair<std::vector<int>, std::vector<int>>> &to_fill);
};