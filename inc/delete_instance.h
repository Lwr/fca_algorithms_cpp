#ifndef FCA_ALGORITHMS_DELETE_INSTANCE_H
#define FCA_ALGORITHMS_DELETE_INSTANCE_H


#include <base_models.h>
#include <map>
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <set>
#include <vector>


class Lattice;

class DeleteInstanceImpl {
    using DestructorByConcept = std::map<Concept *, Concept *>;

    static void fast_deletion(Concept *c, DestructorByConcept &affected_concepts, Lattice &l);
    static void fast_deletion(Concept *c,
                              DestructorByConcept &affected_concepts,
                              std::set<Concept *> &deleted,
                              std::set<Concept *> &modified,
                              Lattice &l);
    static bool confirm_deletion(Concept *c, DestructorByConcept &affected_concepts);
    static bool is_old_concept(Concept *c, DestructorByConcept &affected_concepts);

public:
    static void deleteinstance(int g, Lattice &l, _Context &ctx);
};


#include "lattice.h"
#endif //FCA_ALGORITHMS_DELETE_INSTANCE_H
