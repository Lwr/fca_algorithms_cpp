#ifndef FCA_ALGORITHMS_SOLVER_H
#define FCA_ALGORITHMS_SOLVER_H

#include "base_models.h"
#include "inclose.h"
#include <utility>
#include <vector>

class FCASolver {
protected:
    FCASolver() = default;

public:
    virtual ~FCASolver() {
    }
    virtual std::vector<Concept *> get_concepts(std::vector<std::string> &p_G,
                                                std::vector<std::string> &p_M,
                                                std::vector<std::vector<int>> &p_I) = 0;
    virtual std::vector<Concept *> get_concepts(_Context &p_ctx) = 0;
    // virtual std::vector<std::pair<std::vector<int>, std::vector<Concept>>>
    //     get_lattice(std::vector<std::string>& p_G, std::vector<std::string>& p_M, std::vector<std::vector<int>>& p_I) = 0;
};

class IncloseSolver : public FCASolver {
public:
    IncloseSolver() = default;
    virtual std::vector<Concept *> get_concepts(std::vector<std::string> &p_G,
                                                std::vector<std::string> &p_M,
                                                std::vector<std::vector<int>> &p_I);
    virtual std::vector<Concept *> get_concepts(_Context &p_ctx);
};


#endif //FCA_ALGORITHMS_SOLVER_H