#ifndef FCA_ALGORITHMS_ADDINTENT_H
#define FCA_ALGORITHMS_ADDINTENT_H


#include "lattice.h"
#include <base_models.h>
#include <map>
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <vector>


class AddIntentImpl {
protected:
    static Concept *recursive_addintent(const std::vector<int> &intent,
                                        Concept *generator_concept,
                                        Lattice &concepts,
                                        _Context &ctx,
                                        std::map<Concept *, std::vector<int>> &common_attributes);


public:
    static Concept *addintent(const std::vector<int> &intent,
                              Concept *generator_concept,
                              Lattice &concepts,
                              _Context &ctx);
};

#endif //FCA_ALGORITHMS_ADDINTENT_H
