#ifndef FCA_ALGORITHMS_BASE_MODELS_H
#define FCA_ALGORITHMS_BASE_MODELS_H

// #include <pybind11/pybind11.h>
// #include <pybind11/stl_bind.h>
// #include <map>
#include <list>
#include <string>
#include <utility>
#include <vector>

// namespace py = pybind11;


// Internal Context
class _Context {
public:
    std::vector<std::string> G, M;
    std::vector<std::vector<int>> I;
    _Context() = default;
    _Context(std::vector<std::string> &p_G, std::vector<std::string> &p_M, std::vector<std::vector<int>> &p_I);
    virtual ~_Context() {
    }
    virtual std::string __repr__();
};


// Formal Concept
class Concept {
public:
    std::vector<int> X, Y;
    _Context &ctx;
    std::list<Concept *> parents;
    std::list<Concept *> children;
    //Concept() = default;
    Concept(_Context &p_ctx, const std::vector<int> &p_X, const std::vector<int> &p_Y);
    virtual ~Concept() {
    }

    virtual std::pair<std::vector<std::string>, std::vector<std::string>> to_tuple();
    virtual std::vector<std::string> hr_X();
    virtual std::vector<std::string> hr_Y();
    virtual std::string __repr__();
    virtual void add_child(Concept *child);
    virtual void remove_child(Concept *child);
};


#endif //FCA_ALGORITHMS_BASE_MODELS_H
