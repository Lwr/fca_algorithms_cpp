#ifndef FCA_ALGORITHMS_RULE_H
#define FCA_ALGORITHMS_RULE_H

#include <string>
#include <vector>

class Rule {
    float support;
    float confidence;
    std::vector<std::string> base;
    std::vector<std::string> add;

public:
    Rule(float support, float confidence, std::vector<std::string> base, std::vector<std::string> add);
    virtual ~Rule();
    virtual float get_support();
    virtual float get_confidence();
    virtual std::vector<std::string> get_base();
    virtual std::vector<std::string> get_add();
    virtual std::string __repr__();
};


#endif //FCA_ALGORITHMS_RULE_H