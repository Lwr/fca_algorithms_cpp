#ifndef FCA_ALGORITHMS_LATTICE_H
#define FCA_ALGORITHMS_LATTICE_H


#include "ctx.h"
#include "rule.h"

class Lattice {
protected:
    Concept *_top;
    Concept *_bottom;
    void add_object_with_intent(std::string object_name, const std::vector<int> &intent);
    int get_object_idx(const std::string &object_name);
    int get_attribute_idx(const std::string &object_name);
    std::vector<int> get_intent(int obj_idx);
    std::vector<int> get_extent(int attr_idx);
    void update_bottom(const std::string &attr_name);
    virtual void _get_association_rules(Concept *c,
                                        std::list<std::vector<int>> consequents,
                                        int consequent_size,
                                        std::vector<int> containing,
                                        float min_support,
                                        float min_confidence,
                                        std::vector<Rule *> &rules);
    virtual void _get_association_rules(Concept *c,
                                        std::vector<int> containing,
                                        float min_support,
                                        float min_confidence,
                                        std::vector<Rule *> &rules);
    virtual void initialize();

public:
    // the graph diagram is a vector of tuples where the first element is a concept, the second is
    // the list of elements that we can go from concept, and the third one is the one that reach concept.
    // std::vector<std::tuple<Concept*, std::vector<int>, std::vector<int>>> graph;
    std::vector<Concept *> _concepts;
    Context *ctx;
    Lattice(Context &context);
    static Lattice *copy(Lattice &l);
    virtual ~Lattice();
    void set_bottom(Concept *c);
    void set_top(Concept *c);

    virtual Concept &get_concept(int i);
    virtual Concept &get_top();
    virtual Concept &get_bottom();

    virtual std::vector<Rule *> get_association_rules(std::vector<std::string> containing,
                                                      float min_support = 0.2,
                                                      float min_confidence = 1.0);

    virtual void add_intent(std::string object_name, const std::vector<int> &intent);
    virtual void add_intent_in_bulk(std::vector<std::string> object_names, const std::vector<int> &intent);
    virtual void delete_instance(std::string object_name);
    virtual void add_pair(std::string object_name, std::string attr_name);
    virtual int add_attribute(std::string attr_name);
    virtual int add_object(std::string object_name);
    virtual Lattice *merge_concepts(Lattice &l);
    virtual std::string __repr__();

    friend class AddIntentImpl;
    friend class DeleteInstanceImpl;
};


#endif //FCA_ALGORITHMS_LATTICE_H