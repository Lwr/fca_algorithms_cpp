#ifndef FCA_ALGORITHMS_CTX_H
#define FCA_ALGORITHMS_CTX_H

#include "base_models.h"
#include "solver.h"


class Context : public _Context {
protected:
    virtual void initialize(FCASolver &p_solver);

public:
    FCASolver *solver;
    Context();
    Context(std::vector<std::string> &p_G, std::vector<std::string> &p_M, std::vector<std::vector<int>> &p_I);
    Context(std::vector<std::string> &p_G,
            std::vector<std::string> &p_M,
            std::vector<std::vector<int>> &p_I,
            FCASolver *p_solver);
    virtual ~Context();

    virtual std::vector<Concept *> get_concepts();
};


#endif //FCA_ALGORITHMS_CTX_H