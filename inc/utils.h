#ifndef FCA_ALGORITHMS_UTILS_H
#define FCA_ALGORITHMS_UTILS_H

#include "lattice.h"
#include <base_models.h>
#include <list>
#include <map>
#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>
#include <unordered_map>
#include <vector>


Concept *get_maximal_I_concept(std::vector<int> intent, Concept *generator_concept);


Concept *get_maximal_I_concept(std::vector<int> intent,
                               Concept *generator_concept,
                               const std::map<Concept *, std::vector<int>> &common_attributes);

Concept *get_maximal_E_concept(std::vector<int> extent,
                               Concept *generator_concept,
                               const std::map<Concept *, std::vector<int>> &common_objects);

std::list<Concept *> get_parents(Concept *generator_concept);

std::map<Concept *, std::vector<int>> get_common_attributes(std::vector<int> intent, std::vector<Concept *> &concepts);

std::map<Concept *, std::vector<int>> get_common_objects(std::vector<int> extent, std::vector<Concept *> &concepts);

bool is_there(const std::vector<int> &sorted_array, int elem);

bool is_strictly_included(const std::vector<int> &set, const std::vector<int> &other_set, int common_attributes);

void remove_node(Concept *node, Lattice &l);

void remove_link(Concept *node, Concept *other_node);

void set_link(Concept *node, Concept *other_node);

void erase_number(std::vector<int> &vector, int i);

bool is_included(const std::vector<int> &set, const std::vector<int> &other_set);

void add_ordered(std::vector<int> &sorted_array, int elem);

void add_ordered_if_not_present(std::vector<int> &sorted_array, int elem);

std::vector<int> intersection(std::vector<int> &v1, std::vector<int> &v2);


bool unordered_map_contains(std::unordered_map<std::string, int> m, std::string key);

bool unordered_map_contains_int(std::unordered_map<int, int> m, int key);


#endif //FCA_ALGORITHMS_UTILS_H
