#ifndef FCA_ALGORITHMS_INCLOSE_H
#define FCA_ALGORITHMS_INCLOSE_H

#include <base_models.h>
#include <pybind11/pybind11.h>
#include <vector>


std::vector<Concept *> inclose(_Context &_Context);


#endif //FCA_ALGORITHMS_INCLOSE_H
