from pathlib import Path

try:
    from pybind11.setup_helpers import Pybind11Extension
except ImportError:
    from setuptools import Extension as Pybind11Extension

from pybind11.setup_helpers import build_ext
from setuptools import setup

# read the contents of your README file
from pathlib import Path
this_directory = Path(__file__).parent
long_description = (this_directory / "README.md").read_text()


the_module = Pybind11Extension(
    'fca_algorithms_cpp',
    [str(fname) for fname in Path('src').glob('*.cpp')],
    include_dirs=['inc',],
    extra_compile_args=['-O3', "-std=c++11"]
)

setup(
    name='fca_algorithms_cpp',
    version="0.3.6",
    url='https://gitlab.com/Lwr/fca_algorithms_cpp',
    author='Ramshell',
    author_email='ramshellcinox@gmail.com',
    license="MIT",
    description='Cpp implementation of the lib fca_algorithms',
    long_description=long_description,
    long_description_content_type='text/markdown',
    ext_modules=[the_module],
    cmdclass={"build_ext": build_ext},
    classifiers=[
        "Programming Language :: C++",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'pybind11>=2.10.4',
    ],
)
