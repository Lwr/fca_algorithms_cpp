#include "lattice.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <utility>
#include <vector>


class MergeTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        M = {"Effect", "Power", "Control", "Short"};
        I = {{0, 1, 0, 1}, {1, 0, 1, 1}, {0, 0, 1, 0}, {1, 0, 1, 0}};

        ctx = Context(G, M, I);

        G1 = {"Tg05", "Tg05FX"};
        M1 = {"Effect", "Power", "Control", "Short"};
        I1 = {{0, 1, 0, 1}, {1, 0, 1, 1}};
        G2 = {"Flxtra", "Hxr"};
        M2 = {"Effect", "Power", "Control", "Short"};
        I2 = {{0, 0, 1, 0}, {1, 0, 1, 0}};
        ctx1 = Context(G1, M1, I1);
        ctx2 = Context(G2, M2, I2);
    }

    // void TearDown() override {}

    std::vector<std::string> G, G1, G2;
    std::vector<std::string> M, M1, M2;
    std::vector<std::vector<int>> I, I1, I2;
    Context ctx, ctx1, ctx2;
};


TEST_F(MergeTest, fca_merge_returns_the_exact_amount_of_concepts) {
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {}, expected_result = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    auto l = Lattice(ctx);
    auto l1 = Lattice(ctx1);
    auto l2 = Lattice(ctx2);
    auto merged_l = l1.merge_concepts(l2);

    for (auto c : l._concepts)
        expected_result.push_back(c->to_tuple());

    for (auto c : merged_l->_concepts)
        res.push_back(c->to_tuple());

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);

    EXPECT_EQ(expected_result, res);
}


class MergeTest2 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"o1", "o2"};
        M = {"a", "b", "c"};
        I = {{1, 1, 0}, {0, 1, 1}};

        ctx = Context(G, M, I);

        G1 = {"o1"};
        M1 = {"a", "b"};
        I1 = {{1, 1}};
        G2 = {"o2"};
        M2 = {"b", "c"};
        I2 = {{1, 1}};
        ctx1 = Context(G1, M1, I1);
        ctx2 = Context(G2, M2, I2);
    }


    std::vector<std::string> G, G1, G2;
    std::vector<std::string> M, M1, M2;
    std::vector<std::vector<int>> I, I1, I2;
    Context ctx, ctx1, ctx2;
};


TEST_F(MergeTest2, fca_merge_returns_the_exact_amount_of_concepts_2) {
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {}, expected_result = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    auto l = Lattice(ctx);
    auto l1 = Lattice(ctx1);
    auto l2 = Lattice(ctx2);
    auto merged_l = l1.merge_concepts(l2);

    for (auto c : l._concepts)
        expected_result.push_back(c->to_tuple());

    for (auto c : merged_l->_concepts)
        res.push_back(c->to_tuple());

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);

    EXPECT_EQ(expected_result, res);
}


class MergeTest3 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"0", "1", "2", "3", "4", "5"};
        M = {"a", "b", "c", "d"};
        I = {{0, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 1}, {0, 1, 1, 1}};

        ctx = Context(G, M, I);

        G1 = {"0", "1", "2"};
        M1 = {"a", "b", "c"};
        I1 = {{0, 0, 0}, {0, 0, 1}, {0, 0, 0}};
        G2 = {"3", "4", "5"};
        M2 = {"b", "c", "d"};
        I2 = {{1, 0, 0}, {0, 1, 1}, {1, 1, 1}};
        ctx1 = Context(G1, M1, I1);
        ctx2 = Context(G2, M2, I2);
    }


    std::vector<std::string> G, G1, G2;
    std::vector<std::string> M, M1, M2;
    std::vector<std::vector<int>> I, I1, I2;
    Context ctx, ctx1, ctx2;
};


TEST_F(MergeTest3, fca_merge_returns_the_exact_amount_of_concepts_3) {
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {}, expected_result = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    auto l = Lattice(ctx);
    auto l1 = Lattice(ctx1);
    auto l2 = Lattice(ctx2);
    auto merged_l = l1.merge_concepts(l2);

    for (auto c : l._concepts)
        expected_result.push_back(c->to_tuple());

    for (auto c : merged_l->_concepts)
        res.push_back(c->to_tuple());

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);

    EXPECT_EQ(expected_result, res);
}
