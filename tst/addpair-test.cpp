#include "addintent.h"
#include "lattice.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <utility>
#include <vector>


class IncrementalByPairTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {};
        I = {};

        object_names = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        attr_to_add = {"Effect", "Power", "Control", "Short"};
        stream = {{1, 3}, {0, 2, 3}, {2}, {0, 2}};

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> object_names;
    std::vector<std::string> attr_to_add;
    std::vector<std::vector<int>> stream;
    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


class IncrementalByPairTest2 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {};
        I = {};

        object_names = {"1", "2", "3", "4", "5", "6"};
        attr_to_add = {"a", "b", "c", "d", "e"};
        stream = {
            {0, 2},
            {0, 1},
            {0, 3},
            {1, 3},
            {0},
            {1, 3, 4},
        };

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> object_names;
    std::vector<std::string> attr_to_add;
    std::vector<std::vector<int>> stream;
    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


TEST_F(IncrementalByPairTest, calculates_the_correct_concepts) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"Power", "Short", "Effect", "Control"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05", "Tg05FX"}, {"Short"}},
        {{"Tg05FX"}, {"Short", "Effect", "Control"}},
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };
    Context context = Context(ctx.G, ctx.M, ctx.I);
    Lattice l = Lattice(context);


    for (int i = 0; i < stream.size(); ++i) {
        for (int attr_idx : stream[i]) {
            l.add_pair(object_names[i], attr_to_add[attr_idx]);
        }
    }

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::sort(expected_result.begin(), expected_result.end());
    std::sort(res.begin(), res.end());


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i].second, res[i].second);
}


TEST_F(IncrementalByPairTest2, calculates_the_correct_concepts2) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"a", "c", "b", "d", "e"}},
        {{"1"}, {"a", "c"}},
        {{"1", "2", "3", "4", "5", "6"}, {}},
        {{"1", "2", "3", "5"}, {"a"}},
        {{"2"}, {"a", "b"}},
        {{"2", "4", "6"}, {"b"}},
        {{"3"}, {"a", "d"}},
        {{"3", "4", "6"}, {"d"}},
        {{"4", "6"}, {"b", "d"}},
        {{"6"}, {"b", "d", "e"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };
    Context context = Context(ctx.G, ctx.M, ctx.I);
    Lattice l = Lattice(context);


    for (int i = 0; i < stream.size(); ++i) {
        for (int attr_idx : stream[i]) {
            l.add_pair(object_names[i], attr_to_add[attr_idx]);
        }
    }

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::sort(expected_result.begin(), expected_result.end());
    std::sort(res.begin(), res.end());


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i].second, res[i].second);
}


TEST_F(IncrementalByPairTest2, calculates_the_correct_concepts3) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"a", "c", "b", "d", "e"}},
        {{"1"}, {"a", "c"}},
        {{"1", "2", "3", "4", "5", "6"}, {}},
        {{"1", "2", "3", "5"}, {"a"}},
        {{"2"}, {"a", "b"}},
        {{"2", "4", "6"}, {"b"}},
        {{"3"}, {"a", "d"}},
        {{"3", "4", "6"}, {"d"}},
        {{"4", "6"}, {"b", "d"}},
        {{"6"}, {"b", "d", "e"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.first) < std::make_tuple(b.first) ||
               (std::make_tuple(a.first) == std::make_tuple(b.first) &&
                std::make_tuple(a.second) < std::make_tuple(b.second));
    };
    Context context = Context(ctx.G, ctx.M, ctx.I);
    Lattice l = Lattice(context);


    l.add_pair("1", "a");
    l.add_pair("2", "b");
    l.add_pair("3", "a");
    l.add_pair("4", "b");
    l.add_pair("5", "a");
    l.add_pair("1", "c");
    l.add_pair("3", "d");
    l.add_pair("6", "e");
    l.add_pair("2", "a");
    // l.add_pair("4", "d");
    // l.add_pair("6", "b");
    // l.add_pair("6", "d");

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::vector<std::string> some_G = {"4", "5", "1", "3", "6", "2"};
    std::vector<std::string> some_M = {"a", "b", "c", "d", "e"};
    std::vector<std::vector<int>> some_I = {
        {0, 1, 0, 0, 0},
        {1, 0, 0, 0, 0},
        {1, 0, 1, 0, 0},
        {1, 0, 0, 1, 0},
        {0, 0, 0, 0, 1},
        {1, 1, 0, 0, 0},
    };
    Context some_context = Context(some_G, some_M, some_I);
    Lattice p = Lattice(some_context);

    expected_result.clear();
    // p.add_pair("4", "d");

    for (int i = 0; i < p._concepts.size(); ++i) {
        auto c = p._concepts[i];
        expected_result.push_back(c->to_tuple());
    }


    std::sort(expected_result.begin(), expected_result.end());
    std::sort(res.begin(), res.end());


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i) {
        EXPECT_EQ(expected_result[i].first, res[i].first);
        EXPECT_EQ(expected_result[i].second, res[i].second);
    }
}
