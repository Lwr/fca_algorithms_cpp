#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <vector>
#include <utility>
#include <string>
#include "ctx.h"

using array = std::vector<int>;

class CtxTest : public ::testing::Test {
 protected:
  void SetUp() override {
     G = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
     M = {"Effect", "Power", "Control", "Short"};
     I = {
        {0, 1, 0, 1},
        {1, 0, 1, 1},
        {0, 0, 1, 0},
        {1, 0 ,1, 0}
     };

     ctx = Context(G, M, I);
  }

  // void TearDown() override {}

  std::vector<std::string> G;
  std::vector<std::string> M;
  std::vector<std::vector<int>> I;
  Context ctx;
};

class CtxTest2 : public ::testing::Test {
 protected:
  void SetUp() override {
     G = {"Tg05", "Tg05FX", "Hxr"};
     M = {"Effect", "Power", "Control", "Short"};
     I = {
        {0, 1, 0, 1},
        {1, 0, 1, 1},
        {1, 0 ,1, 0}
     };

     ctx = Context(G, M, I);
  }

  // void TearDown() override {}

  std::vector<std::string> G;
  std::vector<std::string> M;
  std::vector<std::vector<int>> I;
  Context ctx;
};



TEST_F(CtxTest, ctx_get_concepts_returns_all_the_concepts) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05", "Tg05FX"}, {"Short"},},
        {{}, {"Effect", "Power", "Control", "Short"},},
    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;

    for(auto c : ctx.get_concepts()){
        res.push_back(c->to_tuple());
    }

    
    EXPECT_EQ(res.size(), expected_result.size());
    for(int i = 0; i < res.size(); ++i)
        EXPECT_EQ (expected_result[i], res[i]);
}



TEST_F(CtxTest2, ctx_get_concepts_returns_all_the_concepts_2) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{"Tg05", "Tg05FX", "Hxr"}, {}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05", "Tg05FX"}, {"Short"},},
        {{}, {"Effect", "Power", "Control", "Short"},},
    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;

    for(auto c : ctx.get_concepts()){
        res.push_back(c->to_tuple());
    }

    
    EXPECT_EQ(res.size(), expected_result.size());
    for(int i = 0; i < res.size(); ++i)
        EXPECT_EQ (expected_result[i], res[i]);
}
