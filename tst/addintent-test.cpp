#include "addintent.h"
#include "lattice.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <utility>
#include <vector>


class IncrementalTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {"Effect", "Power", "Control", "Short"};
        I = {};

        object_names = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        stream = {{1, 3}, {0, 2, 3}, {2}, {0, 2}};

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> object_names;
    std::vector<std::vector<int>> stream;
    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


TEST_F(IncrementalTest, incremental_returns_all_the_concepts_but_the_top) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"Effect", "Power", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05", "Tg05FX"}, {"Short"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };
    Context context = Context(G, M, I);
    Lattice l = Lattice(context);


    for (int i = 0; i < stream.size(); ++i) {
        AddIntentImpl::addintent(stream[i], l._concepts[0], l, ctx);
    }

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i].second, res[i].second);
}


TEST_F(IncrementalTest, lattice_creation_sets_the_params_correctly) {
    //arrange
    //act
    //assert
    Context context = Context(G, M, I);
    Lattice l = Lattice(context);

    EXPECT_EQ(l._concepts.size(), 1);
    EXPECT_EQ(l.get_concept(0).X, std::vector<int>());

    std::vector<int> intent;
    for (int i = 0; i < M.size(); ++i)
        intent.push_back(i);

    EXPECT_EQ(l.get_concept(0).Y, intent);
}

TEST_F(IncrementalTest, lattice_add_intent_adds_the_necessary_concepts) {
    //arrange
    //act
    //assert
    Context context = Context(G, M, I);
    Lattice l = Lattice(context);

    l.add_intent(object_names[0], stream[0]);

    std::vector<int> expected_extent = {0};
    std::vector<int> expected_intent = stream[0];
    EXPECT_EQ(l.ctx->G.size(), 1);
    EXPECT_EQ(l.ctx->G[0], "Tg05");
    EXPECT_EQ(l.get_concept(1).X, expected_extent);
    EXPECT_EQ(l.get_concept(1).Y, expected_intent);
}


TEST_F(IncrementalTest, lattice_add_intent_multiple_times_until_the_last_one) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"Effect", "Power", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05", "Tg05FX"}, {"Short"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    Context context = Context(G, M, I);
    Lattice l = Lattice(context);

    for (int i = 0; i < stream.size(); ++i) {
        l.add_intent(object_names[i], stream[i]);
    }

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }
    std::sort(res.begin(), res.end(), compare_pairs);

    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


class IncrementalTestSecond : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {
            "a1",
            "a2",
            "a3",
        };
        I = {};

        object_names = {
            "eric",
            "john",
            "mabel",
        };
        stream = {{0, 1}, {2}, {0, 2}};

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> object_names;
    std::vector<std::vector<int>> stream;
    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


TEST_F(IncrementalTestSecond, lattice_add_intent_removes_link_correctly) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{},
         {
             "a1",
             "a2",
             "a3",
         }},
        {{"eric"}, {"a1", "a2"}},
        {{"mabel"},
         {
             "a1",
             "a3",
         }},
        {{"john", "mabel"}, {"a3"}},
        {{
             "eric",
             "john",
             "mabel",
         },
         {}},
        {{"eric", "mabel"}, {"a1"}},
    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    Context context = Context(G, M, I);
    Lattice l = Lattice(context);

    for (int i = 0; i < stream.size(); ++i) {
        l.add_intent(object_names[i], stream[i]);
    }

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }
    std::sort(res.begin(), res.end(), compare_pairs);

    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


TEST_F(IncrementalTest, lattice_creation_with_already_filled_context) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"Effect", "Power", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05", "Tg05FX"}, {"Short"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    std::vector<std::string> new_G(G);
    std::vector<std::string> new_M(M);
    std::vector<std::vector<int>> new_I;
    for (int i = 0; i < object_names.size(); ++i) {
        auto obj_name = object_names[i];
        new_G.push_back(obj_name);
        new_I.push_back(std::vector<int>());
        for (int j = 0; j < new_M.size(); ++j)
            new_I[i].push_back(0);
        for (auto j : stream[i])
            new_I[i][j] = 1;
    }


    Context context = Context(new_G, new_M, new_I);
    Lattice l = Lattice(context);

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i].second, res[i].second);
}


class IncrementalTestSpecificCase : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {"a", "c", "b", "d", "e"};
        I = {};

        object_names = {"1", "2", "3", "4", "5", "6"};
        stream = {
            {0, 1},
            {0, 2},
            {0, 3},
            {2, 3},
            {0},
            {2, 3, 4},
        };

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> object_names;
    std::vector<std::vector<int>> stream;
    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


TEST_F(IncrementalTestSpecificCase, lattice_creation_with_specific_case) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{}, {"a", "c", "b", "d", "e"}},
        {{"1"}, {"a", "c"}},
        {{"1", "2", "3", "4", "5", "6"}, {}},
        {{"1", "2", "3", "5"}, {"a"}},
        {{"2"}, {"a", "b"}},
        {{"2", "4", "6"}, {"b"}},
        {{"3"}, {"a", "d"}},
        {{"3", "4", "6"}, {"d"}},
        {{"4", "6"}, {"b", "d"}},
        {{"6"}, {"b", "d", "e"}},

    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res = {};
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };


    Context context = Context(G, M, I);
    Lattice l = Lattice(context);

    for (int i = 0; i < stream.size(); ++i) {
        l.add_intent(object_names[i], stream[i]);
    }

    for (int i = 0; i < l._concepts.size(); ++i) {
        auto c = l._concepts[i];
        res.push_back(c->to_tuple());
    }

    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);
    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i].second, res[i].second);
}
