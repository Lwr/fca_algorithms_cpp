#include "lattice.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <utility>
#include <vector>

class AssocRuleTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        M = {"Effect", "Power", "Control", "Short"};
        I = {{0, 1, 0, 1}, {1, 0, 1, 1}, {0, 0, 1, 0}, {1, 0, 1, 0}};

        ctx = Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    Context ctx;
};


TEST_F(AssocRuleTest, association_rules_are_well_generated) {
    //arrange
    //act
    //assert
    std::vector<std::tuple<std::vector<std::string>, std::vector<std::string>, float>> expected_result = {
        {{"Effect"}, {"Control"}, 1.0},
        {{"Effect", "Short"}, {"Control"}, 1.0},
        {{"Effect", "Power", "Short"}, {"Control"}, 1.0},
        {{"Effect", "Power", "Control"}, {"Short"}, 1.0},
    };
    std::vector<Rule *> rules = Lattice(ctx).get_association_rules({"Effect"}, 0.0);


    EXPECT_EQ(rules.size(), expected_result.size());
    for (int i = 0; i < rules.size(); ++i) {
        std::tuple<std::vector<std::string>, std::vector<std::string>, float> res = {rules[i]->get_base(),
                                                                                     rules[i]->get_add(),
                                                                                     rules[i]->get_confidence()};
        EXPECT_EQ(expected_result[i], res);
    }
}


TEST_F(AssocRuleTest, association_rules_are_well_generated_confidence_filter) {
    //arrange
    //act
    //assert
    std::vector<std::tuple<std::vector<std::string>, std::vector<std::string>, float>> expected_result = {
        {{"Effect"}, {"Control"}, 1.0},
        {{"Effect", "Short"}, {"Control"}, 1.0},
        {{"Effect", "Control"}, {"Short"}, 0.5},
        {{"Effect", "Power", "Short"}, {"Control"}, 1.0},
        {{"Effect", "Power", "Control"}, {"Short"}, 1.0},
    };
    std::vector<Rule *> rules = Lattice(ctx).get_association_rules({"Effect"}, 0.0, .5);


    EXPECT_EQ(rules.size(), expected_result.size());
    for (int i = 0; i < rules.size(); ++i) {
        std::tuple<std::vector<std::string>, std::vector<std::string>, float> res = {rules[i]->get_base(),
                                                                                     rules[i]->get_add(),
                                                                                     rules[i]->get_confidence()};
        EXPECT_EQ(expected_result[i], res);
    }
}


TEST_F(AssocRuleTest, association_rules_are_well_generated_support_filter) {
    //arrange
    //act
    //assert
    std::vector<std::tuple<std::vector<std::string>, std::vector<std::string>, float>> expected_result = {
        {{"Effect"}, {"Control"}, 1.0},
    };
    std::vector<Rule *> rules = Lattice(ctx).get_association_rules({"Effect"}, 0.5, .5);


    EXPECT_EQ(rules.size(), expected_result.size());
    for (int i = 0; i < rules.size(); ++i) {
        std::tuple<std::vector<std::string>, std::vector<std::string>, float> res = {rules[i]->get_base(),
                                                                                     rules[i]->get_add(),
                                                                                     rules[i]->get_confidence()};
        EXPECT_EQ(expected_result[i], res);
    }
}


class AssocRuleTest2 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"1", "2", "3"};
        M = {"a", "b"};
        I = {{0, 1}, {1, 0}, {1, 1}};

        ctx = Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    Context ctx;
};


TEST_F(AssocRuleTest2, association_rules_calculates_well_the_support) {
    //arrange
    //act
    //assert
    std::vector<std::tuple<std::vector<std::string>, std::vector<std::string>, float, float>> expected_result = {
        {{"a"}, {"b"}, .33, .5},
    };
    std::vector<Rule *> rules = Lattice(ctx).get_association_rules({"a"}, 0.3, .5);


    EXPECT_EQ(rules.size(), expected_result.size());
    for (int i = 0; i < rules.size(); ++i) {
        std::tuple<std::vector<std::string>, std::vector<std::string>, float, float> res = {
            rules[i]->get_base(),
            rules[i]->get_add(),
            float(int(rules[i]->get_support() * 100)) / 100.0,
            rules[i]->get_confidence()};
        EXPECT_EQ(expected_result[i], res);
    }
}


TEST_F(AssocRuleTest2, association_rules_calculates_well_when_no_list_is_provided) {
    //arrange
    //act
    //assert
    std::vector<std::tuple<std::vector<std::string>, std::vector<std::string>, float, float>> expected_result = {
        {{}, {"a"}, .66, .66},
        {{"b"}, {"a"}, .33, .5},
        {{"a"}, {"b"}, .33, .5},
        {{}, {"b"}, .66, .66},
    };
    std::vector<Rule *> rules = Lattice(ctx).get_association_rules({}, 0, 0);


    EXPECT_EQ(rules.size(), expected_result.size());
    for (int i = 0; i < rules.size(); ++i) {
        std::tuple<std::vector<std::string>, std::vector<std::string>, float, float> res = {
            rules[i]->get_base(),
            rules[i]->get_add(),
            float(int(rules[i]->get_support() * 100)) / 100.0,
            float(int(rules[i]->get_confidence() * 100)) / 100.0};
        EXPECT_EQ(expected_result[i], res);
    }
}
