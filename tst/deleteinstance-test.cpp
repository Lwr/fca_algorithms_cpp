#include "lattice.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <string>
#include <utility>
#include <vector>

using array = std::vector<int>;

class DeleteInstanceTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {};
        M = {"Effect", "Power", "Control", "Short"};
        I = {};

        ctx = Context(G, M, I);


        object_names = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        stream = {{1, 3}, {0, 2, 3}, {2}, {0, 2}};


        G2 = {"Tg05", "Tg05FX", "Hxr"};
        M2 = {"Effect", "Power", "Control", "Short"};
        I2 = {{0, 1, 0, 1}, {1, 0, 1, 1}, {1, 0, 1, 0}};

        ctx2 = Context(G2, M2, I2);
    }

    // void TearDown() override {}

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    std::vector<std::string> G2;
    std::vector<std::string> M2;
    std::vector<std::vector<int>> I2;
    Context ctx;
    Context ctx2;
    std::vector<std::string> object_names;
    std::vector<std::vector<int>> stream;
};


TEST_F(DeleteInstanceTest, with_one_object) {
    //arrange
    //act
    //assert
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result;
    for (auto c : ctx2.get_concepts()) {
        expected_result.push_back(c->to_tuple());
    }
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;

    Lattice l = Lattice(ctx);

    for (int i = 0; i < stream.size(); ++i) {
        l.add_intent(object_names[i], stream[i]);
    }

    l.delete_instance("Flxtra");

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }

    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


class DeleteInstanceTest2 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"0", "1", "2"};
        M = {"a", "b", "c"};
        I = {
            {0, 0, 1},
            {1, 1, 0},
            {1, 0, 1},
        };

        ctx = Context(G, M, I);


        to_delete = {"2", "1"};

        G2 = {"0"};
        M2 = {"a", "b", "c"};
        I2 = {{0, 0, 1}};

        ctx2 = Context(G2, M2, I2);
    }

    // void TearDown() override {}

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    std::vector<std::string> G2;
    std::vector<std::string> M2;
    std::vector<std::vector<int>> I2;
    Context ctx;
    Context ctx2;
    std::vector<std::string> to_delete;
};


TEST_F(DeleteInstanceTest2, simple_case) {
    //arrange
    //act
    //assert
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result;
    for (auto c : ctx2.get_concepts()) {
        expected_result.push_back(c->to_tuple());
    }
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;

    Lattice l = Lattice(ctx);

    for (int i = 0; i < to_delete.size(); ++i) {
        l.delete_instance(to_delete[i]);
    }

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }

    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


class DeleteInstanceTest3 : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"0"};
        M = {"a"};
        I = {
            {1},
        };

        ctx = Context(G, M, I);

        to_delete = {"0"};

        G2 = {};
        M2 = {"a"};
        I2 = {};

        ctx2 = Context(G2, M2, I2);
    }

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    std::vector<std::string> G2;
    std::vector<std::string> M2;
    std::vector<std::vector<int>> I2;
    Context ctx;
    Context ctx2;
    std::vector<std::string> to_delete;
};


TEST_F(DeleteInstanceTest3, delete_all_objects) {
    //arrange
    //act
    //assert
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result;
    for (auto c : ctx2.get_concepts()) {
        expected_result.push_back(c->to_tuple());
    }
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;

    Lattice l = Lattice(ctx);

    for (int i = 0; i < to_delete.size(); ++i) {
        l.delete_instance(to_delete[i]);
    }

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }

    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


TEST_F(DeleteInstanceTest3, delete_object_from_the_middle) {
    //arrange
    //act
    //assert
    std::vector<std::string> some_G = {"4", "5", "1", "3", "6", "2"};
    std::vector<std::string> some_M = {"a", "b", "c", "d", "e"};
    std::vector<std::vector<int>> some_I = {
        {0, 1, 0, 0, 0},
        {1, 0, 0, 0, 0},
        {1, 0, 1, 0, 0},
        {1, 0, 0, 1, 0},
        {0, 0, 0, 0, 1},
        {1, 1, 0, 0, 0},
    };
    Context some_context = Context(some_G, some_M, some_I);


    std::vector<std::string> some_G2(some_G);
    std::vector<std::string> some_M2(some_M);
    std::vector<std::vector<int>> some_I2;
    for (auto x : some_I)
        some_I2.push_back(std::vector<int>(x));
    some_G2.erase(some_G2.begin());
    some_I2.erase(some_I2.begin());
    Context some_context_2 = Context(some_G2, some_M2, some_I2);
    auto compare_pairs = [](std::pair<std::vector<std::string>, std::vector<std::string>> a,
                            std::pair<std::vector<std::string>, std::vector<std::string>> b) {
        return std::make_tuple(a.second) > std::make_tuple(b.second);
    };

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result;
    for (auto c : some_context_2.get_concepts()) {
        expected_result.push_back(c->to_tuple());
    }
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);

    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;


    Lattice l = Lattice(some_context);

    l.delete_instance("4");

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }

    std::sort(res.begin(), res.end(), compare_pairs);


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i) {
        EXPECT_EQ(expected_result[i], res[i]);
    }


    some_G = {"5", "1", "3", "6", "2", "4"};
    some_M = {"a", "b", "c", "d", "e"};
    some_I = {
        {1, 0, 0, 0, 0},
        {1, 0, 1, 0, 0},
        {1, 0, 0, 1, 0},
        {0, 0, 0, 0, 1},
        {1, 1, 0, 0, 0},
        {0, 1, 0, 1, 0},
    };
    some_context = Context(some_G, some_M, some_I);
    expected_result.clear();
    for (auto c : some_context.get_concepts()) {
        expected_result.push_back(c->to_tuple());
    }
    std::sort(expected_result.begin(), expected_result.end(), compare_pairs);

    l.add_intent("4", {1, 3});
    res.clear();

    for (auto c : l._concepts) {
        res.push_back(c->to_tuple());
    }

    std::sort(res.begin(), res.end(), compare_pairs);
}
