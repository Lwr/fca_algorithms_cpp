#include "base_models.h"
#include "inclose.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <string>
#include <utility>
#include <vector>

using array = std::vector<int>;

class IncloseTest : public ::testing::Test {
protected:
    void SetUp() override {
        G = {"Tg05", "Tg05FX", "Flxtra", "Hxr"};
        M = {"Effect", "Power", "Control", "Short"};
        I = {{0, 1, 0, 1}, {1, 0, 1, 1}, {0, 0, 1, 0}, {1, 0, 1, 0}};

        ctx = _Context(G, M, I);
    }

    // void TearDown() override {}

    std::vector<std::string> G;
    std::vector<std::string> M;
    std::vector<std::vector<int>> I;
    _Context ctx;
};


TEST_F(IncloseTest, inclose_returns_all_the_concepts_but_the_bottom) {
    //arrange
    //act
    //assert
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> expected_result = {
        {{"Tg05", "Tg05FX", "Flxtra", "Hxr"}, {}},
        {{"Tg05FX", "Hxr"}, {"Effect", "Control"}},
        {{"Tg05FX"}, {"Effect", "Control", "Short"}},
        {{"Tg05"}, {"Power", "Short"}},
        {{"Tg05FX", "Flxtra", "Hxr"}, {"Control"}},
        {{"Tg05", "Tg05FX"}, {"Short"}},
    };
    std::vector<std::pair<std::vector<std::string>, std::vector<std::string>>> res;
    for (auto c : inclose(ctx))
        res.push_back(c->to_tuple());


    EXPECT_EQ(res.size(), expected_result.size());
    for (int i = 0; i < res.size(); ++i)
        EXPECT_EQ(expected_result[i], res[i]);
}


TEST_F(IncloseTest, insert_ordered_test) {
    Concept c = Concept(ctx, {1, 2}, {1, 3});
    EXPECT_EQ(c.__repr__(), "({ Tg05FX, Flxtra }, { Power, Short })");
}
